<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Item class
 */

class Credit extends CI_Model
{

	var $credit_table = 'ospos_credits';
	

	/*
	Determines if a given item_id is an item
	*/
	public function getCredit($denomination){
		$this->db->select('*');
		if($denomination <> ''){
			$this->db->where('id', $denomination);
		}
		$this->db->where('status',1);
		return $this->db->get('ospos_credit_denominations')->result();

		// $query = $this->db->query("SELECT b.id, b.denomination, b.wholesale_price, b.retail_price, b.buying_price FROM ospos_credit_denominations AS b  WHERE b.status = 1");
        // return $query->result();
	}

	function addBamba($data){
		$query = $this->db->insert('ospos_credits_received_packets', $data);
		$insert_id = $this->db->insert_id();
		if($query){
			return $insert_id;
		}else{
			return false;
		}
	}

	public function updateDenomination($id,$data)
	{
		$this->db->where('id', $id);
		$query = $this->db->update('ospos_credit_denominations', $data);
		if($query){
			return true;
		}else{
			return false;
		}
	}

	public function saveIssued($shop, $sno, $user)
	{
		$person = $this->Employee->get_logged_in_employee_info()->person_id;
		//$query2 = $this->db->query("SELECT cards_qty FROM ospos_credit_denominations WHERE denomination = '$denomination'");
		//$row = $query2->row()->cards_qty;
		$query = $this->db->query("UPDATE ospos_credits_received_packet_details set issued = 1, issued_by = '$person', shop = '$shop', date_issued = '".date('Y-m-d')."',issued_to = $user WHERE serial_number = '$sno' and issued = 0");
		if($query){
			return TRUE;
		}else{
			return false;
		}
	}

	public function getCreditDetails()
	{
		$query = $this->db->query("SELECT a.id, a.serial_number, c.denomination FROM ospos_credits_received_packet_details AS a INNER JOIN ospos_credits_received_packets AS b ON a.packet_id = b.id INNER JOIN ospos_credit_denominations as c ON b.denomination = c.id WHERE a.issued = 0");
        return $query->result();
	}

	public function saveRecDetails($sno, $id)
	{
		$den = $this->db->query("SELECT denomination, qty_packets FROM ospos_credits_received_packets WHERE id = '$id' AND status = 1");
        $denomination = $den->row(0)->denomination;
        $qty_packets = $den->row(0)->qty_packets;
		$query = $this->db->query("INSERT into ospos_credits_received_packet_details (serial_number, packet_id, denomination, card_qty) VALUES ('$sno', '$id', '$denomination', '$qty_packets')");
        return TRUE;
	}

	public function getReceivables()
	{
		$query = $this->db->query("SELECT a.id, a.serial_number, c.denomination FROM ospos_credits_received_packet_details as a inner join ospos_credits_received_packets as b on a.packet_id = b.id inner join ospos_credit_denominations as c on b.denomination = c.id");
        return $query->result();
	}

	public function getDenominations(){
		$query = $this->db->query("SELECT id, denomination from ospos_credit_denominations WHERE status = 1");
        return $query->result();
	}

	public function editDenominations($id){
		$query = $this->db->query("SELECT id, denomination, buying_price, wholesale_price, retail_price from ospos_credit_denominations WHERE id = '$id' AND status = 1");
        return $query->result();
	}

	public function getIssued($id, $from,$to, $person, $ttype, $shop, $iss)
	{
		$this->db->where('denomination', $id);
		$this->db->where('status', 1);
		if($person <> '' && $person <> 'all'){
			$this->db->where('issued_to', $person);
		}
		if($shop <> '' && $shop <> 'all'){
			$this->db->where('shop', $shop);
		}
		$this->db->where('issued', $iss);
		// if($from <> '' && $to <> ''){
			$this->db->where('date_issued BETWEEN "'.$from.'" and "'.$to.'"');
		// }
		// $this->db->from('ospos_credits_received_packet_details');
		return $this->db->count_all_results('ospos_credits_received_packet_details');
		//$query = $this->db->query("SELECT count(*) as total FROM ospos_credits_received_packet_details WHERE denomination = '$id' AND issued = '1' and status = 1");
        // return $query->row(0)->total;
        // return $this->db->count_all('ospos_credits_received_packet_details');
	}

	public function transactionTypesW($id, $from,$to, $person, $ttype, $shop)
	{
		$this->db->select_sum('quantity');
        $this->db->select_sum('amount');
        $this->db->where('denomination', $id);
        if($person <> '' && $person <> 'all'){
        	$this->db->where('user_id', $person);
        }
        if($shop <> '' && $shop <> 'all'){
        	$this->db->where('shop', $shop);
        }
        if($ttype <> '' && $ttype <> 'all'){
        	$this->db->where('type', $ttype);
        }
        $this->db->where('datetime BETWEEN "'. date('Y-m-d', strtotime($from)). '" and "'. date('Y-m-d', strtotime($to)).'"');
		$query = $this->db->get('ospos_credit_transactions');
		return $query->row(0);
	}

	public function getTTransactions($id, $from,$to, $person, $ttype, $shop)
	{
		
		//$query = $this->db->query("SELECT SUM(quantity) as total, SUM(amount) AS ttamount FROM ospos_credit_transactions WHERE denomination = '$id' and status = 1 $dquery");
        //return $query->row(0);

        $this->db->select_sum('quantity');
        $this->db->select_sum('amount');
        $this->db->where('denomination', $id);
        if($person <> '' && $person <> 'all'){
        	$this->db->where('user_id', $person);
        }
        if($shop <> '' && $shop <> 'all'){
        	$this->db->where('shop', $shop);
        }
        if($ttype <> '' && $ttype <> 'all'){
        	$this->db->where('type', $ttype);
        }
        $this->db->where('datetime BETWEEN "'. date('Y-m-d', strtotime($from)). '" and "'. date('Y-m-d', strtotime($to)).'"');
		$query = $this->db->get('ospos_credit_transactions');
		return $query->row(0);
	}

	public function transactionsSummary()
	{
		$query = $this->db->query("SELECT a.id, ct.credit_type, b.denomination, a.amount, c.first_name, c.last_name, a.status FROM ospos_credit_transactions AS a INNER JOIN ospos_credit_denominations AS b ON a.denomination = b.id INNER JOIN ospos_people AS c ON a.user_id = c.person_id INNER JOIN ospos_credit_type AS ct ON a.type = ct.id");
        return $query->result();
	}

	public function getAlTransactions($from, $to, $person, $ttype, $shop)
	{
		$this->db->select('*');
		$this->db->from('ospos_credit_transactions AS a');
		$this->db->join('ospos_credit_denominations AS b', 'a.denomination = b.id', 'INNER');
		$this->db->join('ospos_people AS c', 'a.user_id = c.person_id', 'INNER');
		$this->db->join('ospos_credit_type AS d', 'a.type = d.id', 'INNER');

		if($from!='' && $to!='' || $from!= NULL) // To process our custom input parameter
        {

            $this->db->where('a.datetime BETWEEN "'. date('Y-m-d', strtotime($from)). '" and "'. date('Y-m-d', strtotime($to)).'"');
        }

        if($person != '' && $person !== 'all'){
        	$this->db->where('a.user_id = "'.$person.'"');	
        }

        if($shop != '' && $shop !== 'all'){
        	$this->db->where('a.shop = "'.$shop.'"');	
        }

        if($ttype != '' && $ttype !== 'all'){
        	$this->db->where('a.type = "'.$ttype.'"');	
        }

		$query = $this->db->get();
		return $query->result();
	}

	public function getAllTransactions($startDate, $endDate)
	{
		$query = $this->db->query("SELECT a.id, ct.credit_type, b.denomination, a.amount, c.first_name, c.last_name, a.status FROM ospos_credit_transactions AS a INNER JOIN ospos_credit_denominations AS b ON a.denomination = b.id INNER JOIN ospos_people AS c ON a.user_id = c.person_id INNER JOIN ospos_credit_type AS ct ON a.type = ct.id");
        return $query->result();
	}

	public function getPersons()
	{
		$query = $this->db->query("SELECT distinct a.user_id, b.* FROM ospos_credit_transactions as a inner join ospos_people AS b ON a.user_id = b.person_id");
		return $query->result_array();
	}

	public function get_undeleted()
	{
		$this->db->from('ospos_stock_locations');
		$this->db->where('deleted', 0);

		$q = $this->db->get();
		if($q->num_rows() > 0)
        {
          foreach ($q->result() as $row)
          {
            $data[] = $row;
          }
          return $data;
        }
	}

	public function getShops()
		{
			$query = $this->db->query("SELECT * FROM ospos_stock_locations WHERE deleted = 0");
			return $query->result_array();
		}

	public function allTransactions($startDate, $endDate, $person, $t_type)
	{
		$sdate = '';
		if($startDate !== '' && $endDate !== ''){
			$sdate = " WHERE date_format(a.`datetime`, '%Y-%m-%d') BETWEEN '$startDate' AND '$endDate'";
		}else{
			$sdate = "";
		}

		if($person !== ''){
			$per = " AND a.user_id = '$person'";
		}else{
			$per = "";
		}

		if($t_type !== ""){
			$ttype = " AND a.type = '$ttype'";
		}else{
			$ttype = "";
		}

		$query = $this->db->query("SELECT a.id, ct.credit_type, b.denomination, date_format(a.`datetime`, '%Y-%m-%d') as datetime, a.amount, c.first_name, c.last_name, a.status FROM ospos_credit_transactions AS a INNER JOIN ospos_credit_denominations AS b ON a.denomination = b.id INNER JOIN ospos_people AS c ON a.user_id = c.person_id INNER JOIN ospos_credit_type AS ct ON a.type = ct.id");
        return $query->result_array();
	}

	public function getDenQty($den)
	{
		$query = $this->db->query("SELECT count(d.id) as total FROM ospos_credits_received_packet_details d inner join ospos_credits_received_packets p on d.packet_id = p.id where d.issued = 0 and p.denomination = '$den' and d.status = 1");
        return $query->row(0);
	}

	public function editCredit($id){
		$query = $this->db->query("SELECT a.id, a.credit, a.selling_price, a.buying_price FROM ospos_credits AS a WHERE a.status = 1 AND a.id = '$id'");
        return $query->row_array();
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		$query = $this->db->update('ospos_credits', $data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
}
?>
