<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Report.php");

/**
 * 
 */
class Phones_report extends Report
{
	public function getDataColumns()
	{
		return array(
			'summary' => array(
				array('item_id' => $this->lang->line('reports_item')),
				array('name' => $this->lang->line('reports_item_name')),
				array('location_name'	=>	$this->lang->line('reports_stock_location')),
				array('quantity_available'	=>	$this->lang->line('reports_quantity_available')),
				array('returns_to_supplier'	=>	$this->lang->line('reports_returns_to_supplier')),
				array('receivedRequisition'	=>	$this->lang->line('reports_received_requisitions')),
				array('issuedRequisition'	=>	$this->lang->line('reports_issued_requisitions')),
				array('sales'				=>	$this->lang->line('reports_sales'))
			),
			'details' => array(
				$this->lang->line('reports_item'),
				$this->lang->line('reports_item_name'),
				$this->lang->line('reports_category'),
				$this->lang->line('reports_description'),
				$this->lang->line('reports_quantity'),
				$this->lang->line('reports_sn_range'))
		);
	}

	public function getData(array $inputs)
	{
		
		$this->db->select('
			item_quantities.item_id,
			items.name,
			items.category,
			stock_locations.location_name,
			item_quantities.quantity
			');
		$this->db->from('item_quantities');
		if($inputs['location_id'] !== 'all'){
			$this->db->where('item_quantities.location_id', $inputs['location_id']);
		}
		$this->db->join('items', 'items.item_id = item_quantities.item_id');
		$this->db->where('items.category', 'Phone');
		$this->db->join('stock_locations', 'stock_locations.location_id = item_quantities.location_id');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		$data['details'] = array();

		foreach($data['summary'] as $key=>$value)
		{
			
		}
		return $data;
	}

	public function getSummaryData(array $inputs)
	{
		$this->db->select('SUM(ospos_item_quantities.quantity) AS quantity_available');
		$this->db->from('item_quantities');
		if($inputs['location_id'] !== 'all'){
			$this->db->where('item_quantities.location_id', $inputs['location_id']);
		}
		$this->db->join('items', 'items.item_id = item_quantities.item_id');
		$this->db->where('items.category', 'Phone');
		$this->db->join('stock_locations', 'stock_locations.location_id = item_quantities.location_id');

		return $this->db->get()->row_array();
	}

	public function toSupplierReturns($item_id, array $inputs)
	{
		$this->db->select('SUM(quantity_purchased) AS quantity_returned');
		$this->db->from('receivings_items');
		$this->db->join('receivings', 'receivings.receiving_id = receivings_items.receiving_id');
		$this->db->where('receivings_items.quantity_purchased < 0');
		$this->db->where('receivings.mode', 'return');
		$this->db->where('receivings_items.item_id', $item_id);
		if($inputs['location_id'] !== 'all'){
			$this->db->where('receivings_items.item_location', $inputs['location_id']);
		}
		$this->db->where('DATE(ospos_receivings.receiving_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		return $this->db->get()->row()->quantity_returned;
	}

	public function requisition(array $inputs, $item_id, $requisition)
	{
		$this->db->select('SUM(quantity_purchased) AS requisition');
		$this->db->from('receivings_items');
		$this->db->where('receivings_items.item_id', $item_id);
		if($inputs['location_id'] !== 'all'){
			$this->db->where('receivings_items.item_location', $inputs['location_id']);
		}
		$this->db->join('receivings', 'receivings.receiving_id = receivings_items.receiving_id');
		$this->db->where('DATE(ospos_receivings.receiving_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		$this->db->where('receivings.mode', 'requisition');
		if($requisition == 'to'){
			$this->db->where('receivings_items.quantity_purchased > 0');
		}elseif ($requisition == 'from') {
			$this->db->where('receivings_items.quantity_purchased < 0');
		}

		return $this->db->get()->row()->requisition;
	}

	public function sales(array $inputs, $item_id)
	{
		$this->db->select('SUM(quantity_purchased) AS quantity_sold');
		$this->db->from('sales_items');
		$this->db->where('item_id', $item_id);
		if($inputs['location_id'] !== 'all'){
			$this->db->where('sales_items.item_location', $inputs['location_id']);
		}
		$this->db->join('sales', 'sales.sale_id = sales_items.sale_id');
		$this->db->where('DATE(ospos_sales.sale_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		return $this->db->get()->row()->quantity_sold;
	}
}