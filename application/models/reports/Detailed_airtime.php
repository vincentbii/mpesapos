<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Report.php");

/**
 * 
 */
class Detailed_airtime extends Report
{
	public function create(array $inputs)
	{
		//Create our temp tables to work with the data in our report
		$this->Sale->create_temp_table($inputs);
	}

	public function getDataColumns()
	{
		return array(
			'summary' => array(
				array('id' => $this->lang->line('reports_item')),
				array('receiving_date' => $this->lang->line('reports_date')),
				array('name' => $this->lang->line('reports_item_name')),
				array('quantity_purchased' => $this->lang->line('reports_quantity')),
				array('sn_range' => $this->lang->line('reports_sn_range'))
			),
			'details' => array(
				$this->lang->line('reports_item'),
				$this->lang->line('reports_item_name'),
				$this->lang->line('reports_category'),
				$this->lang->line('reports_description'),
				$this->lang->line('reports_quantity'),
				$this->lang->line('reports_sn_range'))
		);
	}

	public function getData(array $inputs)
	{

		$this->db->select('items.name, items.category, receivings_items.item_id, serialnumber, items.description, quantity_purchased, sn_max, sn_min, receivings.receiving_time');
		$this->db->from('receivings_items');
		$this->db->join('items', 'items.item_id = receivings_items.item_id');
		$this->db->where('items.category', 'Airtime');
		$this->db->join('receivings', 'receivings.receiving_id = receivings_items.receiving_id');
		$this->db->where('DATE(ospos_receivings.receiving_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		$this->db->where('receivings.mode', 'receive');
		if($inputs['employee_id'] !== 'all'){
			$this->db->where('receivings.employee_id', $inputs['employee_id']);
		}

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		$data['details'] = array();

		foreach($data['summary'] as $key=>$value)
		{
		}
		return $data;
	}

	public function getSummaryData(array $inputs)
	{
		$this->db->select('SUM(quantity_purchased) AS quantity_purchased');
		$this->db->from('receivings_items');
		$this->db->join('receivings', 'receivings_items.receiving_id = receivings.receiving_id');
		$this->db->where('receivings.employee_id', $inputs['employee_id']);
		$this->db->where('receivings.mode', 'receive');
		$this->db->where('DATE(ospos_receivings.receiving_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		$this->db->join('items', 'items.item_id = receivings_items.item_id');
		$this->db->where('items.category', 'Airtime');

		return $this->db->get()->row_array();
	}
}