<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Report.php");

/**
 * 
 */
class Detailed_items extends Report
{
	public function create(array $inputs)
	{
		//Create our temp tables to work with the data in our report
		$this->create_temp_table($inputs);
	}
}