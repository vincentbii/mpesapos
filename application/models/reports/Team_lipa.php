<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Report.php");

/**
 * 
 */
class Team_lipa extends Report
{
	public function getDataColumns()
	{
		return array(
			'summary' => array(
				array('item_id' => $this->lang->line('reports_item')),
				array('name' => $this->lang->line('reports_item_name')),
				array('employee'	=>	$this->lang->line('reports_employee')),
				array('quantity' 	=> $this->lang->line('reports_quantity')),
				array('successful'	=>	$this->lang->line('reports_successful')),
				array('installed'	=>	$this->lang->line('reports_installed')),
				array('pending'	=>	$this->lang->line('reports_pending')),
				array('rejected'	=>	$this->lang->line('reports_rejected'))
			),
			'details' => array(
				$this->lang->line('reports_item'),
				$this->lang->line('reports_item_name'),
				$this->lang->line('reports_category'),
				$this->lang->line('reports_description'),
				$this->lang->line('reports_quantity'),
				$this->lang->line('reports_sn_range'))
		);
	}

	public function getData(array $inputs)
	{
		//get services offered by Team Lipa group
		$this->db->select('
			sales_items.sale_id,
			sales_items.quantity_purchased,
			sales_items.installed,
			sales_items.pending,
			sales_items.successful,
			sales_items.rejected,
			items.item_id,
			items.name,
			items.category,
			people.first_name,
			people.last_name
			');
		$this->db->from('sales_items');
		$this->db->join('items', 'items.item_id = sales_items.item_id');
		$this->db->where('items.category', 'Team Lipa');
		$this->db->join('sales', 'sales.sale_id = sales_items.sale_id');
		if($inputs['employee_id'] !== 'all'){
			$this->db->where('employee_id', $inputs['employee_id']);
		}
		$this->db->join('people', 'people.person_id = sales.employee_id');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		$data['details'] = array();

		foreach($data['summary'] as $key=>$value)
		{
			// $this->db->select('items.name, items.category, receivings_items.item_id, serialnumber, items.description, quantity_purchased, receivings_items.receiving_quantity, sn_max, sn_min');
			// $this->db->from('receivings_items');
			// $this->db->where('receiving_id', $value['receiving_id']);
			// $this->db->join('items', 'items.item_id = receivings_items.item_id');
			// $this->db->where('items.category', 'Airtime');
			// $data['details'][$key] = $this->db->get()->result_array();
		}
		return $data;
	}

	public function getSummaryData(array $inputs)
	{
		//Get summary of team lipa services
		$this->db->select('
			SUM(quantity_purchased) AS quantity_purchased,
			SUM(successful) AS successful,
			SUM(installed) AS installed,
			SUM(pending) AS pending,
			SUM(rejected) AS rejected
			');
		$this->db->from('sales_items');
		$this->db->join('items', 'items.item_id = sales_items.item_id');
		$this->db->where('items.category', 'Team Lipa');
		$this->db->join('sales', 'sales.sale_id = sales_items.sale_id');
		if($inputs['employee_id'] !== 'all'){
			$this->db->where('employee_id', $inputs['employee_id']);
		}

		return $this->db->get()->row_array();
	}
}