<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Report.php");

/**
 * 
 */
class Airtime_summary extends Report
{
	public function getDataColumns()
	{
		return array(
			'summary' => array(
				array('item_id' => $this->lang->line('reports_item')),
				array('name' => $this->lang->line('reports_item_name')),
				array('quantity_issued' => $this->lang->line('reports_quantity_issued')),
				// array('sn_range' => $this->lang->line('reports_sn_range'). ' Issued'),
				array('quantity_sold' => $this->lang->line('reports_quantity_sold'))
			),
			'details' => array(
				$this->lang->line('reports_item'),
				$this->lang->line('reports_item_name'),
				$this->lang->line('reports_category'),
				$this->lang->line('reports_description'),
				$this->lang->line('reports_quantity'),
				$this->lang->line('reports_sn_range'))
		);
	}

	public function getData(array $inputs)
	{
		if($inputs['employee_id'] !== 'all'){
			$employee_id = "AND receivings.person = '".$inputs['employee_id']."'";
		}else{
			$employee_id = "";
		}

		if($inputs['location_id'] !== 'all'){
			$location = " AND item_location = '".$inputs['location_id']."'";
		}else{
			$location = "";
		}
		$this->db->select('
			items.item_id,
			items.name,
			SUM(ospos_receivings_items.quantity_purchased) AS quantity_issued
			');
		$this->db->from('items');
		$this->db->where('category', 'Airtime');
		$this->db->join('receivings_items', 'receivings_items.item_id = items.item_id AND receivings_items.quantity_purchased > 0 '.$location);
		$this->db->join('receivings', 'receivings.receiving_id = receivings_items.receiving_id AND receivings.mode = "requisition" '.$employee_id.' AND DATE(ospos_receivings.receiving_time) BETWEEN ' . $this->db->escape($inputs['start_date']) .' AND '. $this->db->escape($inputs['end_date']));
		$this->db->group_by('items.item_id');
		$this->db->order_by('items.item_id');
		// $this->db->

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		$data['details'] = array();

		foreach($data['summary'] as $key=>$value)
		{
			
		}
		return $data;
	}

	public function getSummaryData(array $inputs)
	{
		$this->db->select('SUM(quantity_purchased) AS quantity_purchased');
		$this->db->from('receivings_items');
		$this->db->join('receivings', 'receivings_items.receiving_id = receivings.receiving_id');
		$this->db->where('receivings.employee_id', $inputs['employee_id']);
		$this->db->where('receivings.mode', 'receive');
		$this->db->where('DATE(ospos_receivings.receiving_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		$this->db->join('items', 'items.item_id = receivings_items.item_id');
		$this->db->where('items.category', 'Airtime');

		return $this->db->get()->row_array();
	}

	public function getSoldItems($item_id='', array $inputs)
	{
		//get sales of this item
		$this->db->select('SUM(quantity_purchased) AS quantity_sold');
		$this->db->from('sales_items');
		$this->db->where('item_id', $item_id);
		if($inputs['location_id'] !== 'all'){
			$this->db->where('item_location', $inputs['location_id']);
		}
		$this->db->join('sales', 'sales.sale_id = sales_items.sale_id');
		$this->db->where('DATE(ospos_sales.sale_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		if($inputs['employee_id'] !== 'all'){
			$this->db->where('employee_id', $inputs['employee_id']);
		}
		return $this->db->get()->row()->quantity_sold;
	}

}