<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once("Report.php");

/**
 * 
 */
class Sold_airtime extends Report
{
	public function getDataColumns()
	{
		return array(
			'summary' => array(
				array('id' => $this->lang->line('reports_item')),
				array('sale_date' => $this->lang->line('reports_date')),
				array('sales_person'	=> $this->lang->line('reports_employee')),
				array('name' => $this->lang->line('reports_item_name')),
				array('quantity_sold' => $this->lang->line('reports_quantity')),
				array('sn_range' => $this->lang->line('reports_sn_range'))
			),
			'details' => array(
				$this->lang->line('reports_item'),
				$this->lang->line('reports_item_name'),
				$this->lang->line('reports_category'),
				$this->lang->line('reports_description'),
				$this->lang->line('reports_quantity'),
				$this->lang->line('reports_sn_range'))
		);
	}


	public function getData(array $inputs)
	{
		$this->db->select('sales_items.item_id,sales_items.quantity_purchased, sales_items.sn_min, sales_items.sn_max, sales.sale_id, sales.sale_time, people.first_name, people.last_name, items.item_id, items.name');
		$this->db->from('sales_items');
		$this->db->join('sales', 'sales.sale_id = sales_items.sale_id');
		$this->db->where('DATE(ospos_sales.sale_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		if($inputs['employee_id'] !== 'all'){
			$this->db->where('sales.employee_id', $inputs['employee_id']);
		}
		$this->db->join('people', 'people.person_id = sales.employee_id', 'left');
		$this->db->join('items', 'sales_items.item_id = items.item_id');
		$this->db->where('items.category', 'Airtime');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		$data['details'] = array();

		foreach($data['summary'] as $key=>$value)
		{


		}
		return $data;
	}

	public function getSummaryData(array $inputs)
	{
		$this->db->select('SUM(quantity_purchased) AS quantity_sold');
		$this->db->from('sales_items');
		$this->db->join('sales', 'sales.sale_id = sales_items.sale_id');
		$this->db->where('DATE(ospos_sales.sale_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		$this->db->where('sales.employee_id', $inputs['employee_id']);
		$this->db->join('people', 'people.person_id = sales.employee_id', 'left');
		$this->db->join('items', 'sales_items.item_id = items.item_id');
		$this->db->where('items.category', 'Airtime');

		return $this->db->get()->row_array();
	}
}