<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Report.php");
/**
 * 
 */
class Confirmation extends Report
{
	public function getDataColumns()
	{
		return array(
			'summary' => array(
				array('id' => $this->lang->line('reports_item')),
				array('confirmation_date'	=>	$this->lang->line('reports_date')),
				array('location' => $this->lang->line('reports_stock_location')),
				array('employee' => $this->lang->line('reports_employee')),
				array('comment' => $this->lang->line('reports_comment')),
				array('status' => $this->lang->line('reports_status')),
				array('sup_signature'	=>	'Supervisor '.$this->lang->line('reports_signature')),
				array('att_signature'	=>	'Shop Attendant '.$this->lang->line('reports_signature'))
			),
			'details' => array(
				$this->lang->line('reports_item'),
				$this->lang->line('reports_item_name'),
				$this->lang->line('reports_category'),
				$this->lang->line('reports_description'),
				$this->lang->line('reports_quantity'),
				$this->lang->line('reports_sn_range'))
		);
	}

	public function getConfirmation($inputs)
	{
		$this->db->where('location_id',$inputs['location_id']);
		$this->db->where('date_format(confirmation_date, "%Y-%m-%d")',$inputs['location_id']);
	   $q = $this->db->get('stock_confirmation');

	   if ( $q->num_rows() < 1 ) 
	   {
	      // $this->db->where('user_id',$id);
	   	$this->db->set('location_id', $inputs['location_id']);
	   	$this->db->set('confirmation_date', date('Y-m-d H:m:s'));
	      $this->db->insert('stock_confirmation');
	   }
	}

	public function getData(array $inputs)
	{
		$this->db->select('
			stock_locations.location_id,
			stock_locations.location_name,
			stock_confirmation.id,
			stock_confirmation.comment,
			IF(ospos_stock_confirmation.status = 1, "Confirmed", "Not Confirmed") AS status,
			people.first_name,
			people.last_name,
			stock_confirmation.confirmation_date,
			stock_confirmation.sup_signature,
			stock_confirmation.att_signature
			');
		$this->db->from('stock_locations');
		$this->db->where('deleted', FALSE);
		$this->db->join('stock_confirmation', 'stock_confirmation.location_id = stock_locations.location_id AND DATE(ospos_stock_confirmation.confirmation_date) BETWEEN '.$this->db->escape($inputs['start_date']).' AND '.$this->db->escape($inputs['end_date']), 'left');
		// $this->db->where('DATE(ospos_stock_confirmation.confirmation_date) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']), 'left');
		$this->db->join('people', 'people.person_id = stock_confirmation.employee_id', 'left');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		$data['details'] = array();

		foreach($data['summary'] as $key=>$value)
		{

		}
		return $data;
	}

	public function getSummaryData(array $inputs)
	{
		// $this->db->select('SUM(quantity_purchased) AS quantity_purchased');
		// $this->db->from('receivings_items');
		// $this->db->join('receivings', 'receivings_items.receiving_id = receivings.receiving_id');
		// $this->db->where('receivings.mode', 'receive');
		// $this->db->where('DATE(ospos_receivings.receiving_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		// $this->db->join('items', 'items.item_id = receivings_items.item_id');
		// $this->db->where('items.category', 'Airtime');

		// return $this->db->get()->row_array();
	}
}