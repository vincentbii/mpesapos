<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Report.php");

/**
 * 
 */
class Issued_airtime extends Report
{
	public function getDataColumns()
	{
		return array(
			'summary' => array(
				array('id' => $this->lang->line('reports_item')),
				array('receiving_date' => $this->lang->line('reports_date')),
				array('issued_to'	=>	$this->lang->line('reports_employee')),
				array('name' => $this->lang->line('reports_item_name')),
				array('quantity_purchased' => $this->lang->line('reports_quantity')),
				array('sn_range' => $this->lang->line('reports_sn_range'))
			),
			'details' => array(
				$this->lang->line('reports_item'),
				$this->lang->line('reports_item_name'),
				$this->lang->line('reports_category'),
				$this->lang->line('reports_description'),
				$this->lang->line('reports_quantity'),
				$this->lang->line('reports_sn_range'))
		);
	}

	public function getData(array $inputs)
	{
		$this->db->select('items.name, items.category, receivings_items.item_id, serialnumber, items.description, quantity_purchased, sn_max, sn_min, receivings.receiving_time, receivings.person, people.first_name, people.last_name');
		$this->db->from('receivings_items');
		$this->db->where('quantity_purchased > 0');
		$this->db->join('items', 'items.item_id = receivings_items.item_id');
		$this->db->where('items.category', 'Airtime');
		$this->db->join('receivings', 'receivings.receiving_id = receivings_items.receiving_id');
		$this->db->where('DATE(ospos_receivings.receiving_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		$this->db->where('receivings.mode', 'requisition');
		if($inputs['employee_id'] !== 'all'){
			$this->db->where('receivings.person', $inputs['employee_id']);
		}
		$this->db->join('people', 'people.person_id = receivings.person', 'left');
		// $this->db->group_by('items.category');

		$data = array();
		$data['summary'] = $this->db->get()->result_array();
		$data['details'] = array();

		foreach($data['summary'] as $key=>$value)
		{
			// $this->db->select('items.name, items.category, receivings_items.item_id, serialnumber, items.description, quantity_purchased, receivings_items.receiving_quantity, sn_max, sn_min');
			// $this->db->from('receivings_items');
			// $this->db->where('receiving_id', $value['receiving_id']);
			// $this->db->join('items', 'items.item_id = receivings_items.item_id');
			// $this->db->where('items.category', 'Airtime');
			// $data['details'][$key] = $this->db->get()->result_array();
		}
		return $data;
	}

	public function getSummaryData(array $inputs)
	{
		$this->db->select('SUM(quantity_purchased) AS quantity_issued');
		$this->db->from('receivings_items');
		$this->db->where('quantity_purchased > 0');
		$this->db->join('items', 'items.item_id = receivings_items.item_id');
		$this->db->where('items.category', 'Airtime');
		$this->db->join('receivings', 'receivings.receiving_id = receivings_items.receiving_id');
		$this->db->where('DATE(ospos_receivings.receiving_time) BETWEEN ' . $this->db->escape($inputs['start_date']) . ' AND ' . $this->db->escape($inputs['end_date']));
		$this->db->where('receivings.mode', 'requisition');
		$this->db->where('receivings.person', $inputs['employee_id']);

		return $this->db->get()->row_array();
	}
}