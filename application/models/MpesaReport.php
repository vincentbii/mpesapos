<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class MpesaReport extends CI_Model{
  public function get_last_ten_entries(){
        $query = $this->db->get('mpesa_report', 10);
        return $query->result();
    }

    public function disable($value='')
    {
      // $this->db->where('date_format(report_date, "%Y-%m-%d") < "'.date('Y-m-d').'"');
      // $this->db->update()

      $this->db->set('status', 0);
      $this->db->where('date_format(report_date, "%Y-%m-%d") < "'.date('Y-m-d').'"');
      $this->db->update('mpesa_report');
    }

  public function getReport($module_id = 'items')
  {
    $this->db->select('stock_locations.location_name, stock_locations.location_id, mpesa_report.*');
    $this->db->from('stock_locations');
    $this->db->join('permissions AS permissions', 'permissions.location_id = stock_locations.location_id');
    $this->db->join('grants AS grants', 'grants.permission_id = permissions.permission_id');
    $this->db->where('person_id', $this->session->userdata('person_id'));
    $this->db->like('permissions.permission_id', $module_id, 'after');
    $this->db->where('deleted', 0);
    $this->db->join('mpesa_report AS mpesa_report', 'mpesa_report.shop = stock_locations.location_id', 'left');
    $this->db->where('mpesa_report.status', 1);
    $q = $this->db->get();
    if($q->num_rows() > 0)
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
  }

  public function edit($id='', $data='')
  {
    $this->db->where('shop',$id);
     $q = $this->db->get('mpesa_report');

     if ( $q->num_rows() > 0 ) 
     {
        $this->db->where('shop',$id);
        $this->db->update('mpesa_report',$data);
        return "Report Updated Successfully!";
     } else {
        $this->db->set('shop', $id);
        $this->db->insert('mpesa_report',$data);
        return "Report Inserted Successfully!";
     }


  }

  public function createEntries($value='')
  {
    $this->db->where('shop',$value->location_id);
    $this->db->where('report_date', date("Y-m-d"));
    $q = $this->db->get('mpesa_report');
    // $this->db->query_reset();

     if ( $q->num_rows() < 1 ) 
     {
      $this->db->set('shop', $value->location_id);
      $this->db->set('user', $this->session->userdata('person_id'));
      $this->db->set('report_date', date('Y-m-d'));
      return $this->db->insert('mpesa_report');
     }
     // return FALSE;
    
  }
}