<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Library_admin extends CI_Model
{
	
	public function get_total_rows()
	{
		$this->db->from('library_categories');
		$this->db->where('deleted', 0);

		return $this->db->count_all_results();
	}

	public function getFiles($value='')
	{
		$this->db->select('library_files.id, library_files.description, library_files.external_link, library_files.allow_links, library_files.name, library_categories.name AS category, library_categories.id AS category_id');
		$this->db->from('library_files');
		$this->db->join('library_categories', 'library_categories.id = library_files.category');
		return $this->db->get()->result();
	}

	public function getCategories($value='')
	{
		$this->db->from('library_categories');
		if($value !== ''){
			$this->db->where('id', $value);
		}
		return $this->db->get()->result();
	}

	public function destroyFile($id='')
	{
		return $this->db->delete('library_files', array('id' => $id));
	}

	public function updateCategory($id='', $data = '')
	{
		$this->db->where('id', $id);
		return $this->db->update('library_categories', $data);
	}

	public function updateFile($id='', $data = '')
	{
		$this->db->where('id', $id);
		return $this->db->update('library_files', $data);
	}

	public function storeCategory($id='', $data = '')
	{
		$this->db->where('id', $id);
		return $this->db->insert('library_categories', $data);
	}

	public function addFile($id='', $data = '')
	{
		$this->db->where('id', $id);
		return $this->db->insert('library_files', $data);
	}

	public function addLink($value='')
	{
		return $this->db->insert('library_files', $value);
	}

	public function destroy($id='')
	{
		$delete = $this->db->delete('library_categories', array('id' => $id));
		if($delete){
			return "Category deleted successfully!";
		}else{
			return "Category not deleted. Delete dependent files.";
		}
	}
}