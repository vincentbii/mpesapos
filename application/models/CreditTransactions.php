<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Item class
 */

class CreditTransactions extends CI_Model
{

	var $credit_table = 'ospos_credit_transactions';

	/*
	Determines if a given item_id is an item
	*/
	public function getCreditTransactions(){
		$person = $this->Employee->get_logged_in_employee_info()->person_id;
		$query = $this->db->query("SELECT a.id, b.denomination, a.quantity, date_format(a.`datetime`, '%d-%m-%Y') as datetime, a.amount, c.first_name, c.last_name, t.credit_type FROM ospos_credit_transactions AS a INNER JOIN ospos_credit_denominations AS b ON a.denomination = b.id INNER JOIN ospos_people AS c ON a.user_id = c.person_id inner join ospos_credit_type as t on a.type = t.id WHERE c.person_id = '$person'");
        return $query->result();
	}

	public function getCredit($denomination){
		$this->db->select('*');
		if($denomination <> ''){
			$this->db->where('id', $denomination);
		}
		$this->db->where('status',1);
		return $this->db->get('ospos_credit_denominations')->result();

		// $query = $this->db->query("SELECT b.id, b.denomination, b.wholesale_price, b.retail_price, b.buying_price FROM ospos_credit_denominations AS b  WHERE b.status = 1");
        // return $query->result();
	}

	public function edit($id){
		$query = $this->db->query("SELECT a.id, a.amount, b.credit, b.id as cid FROM ospos_credit_transactions as A INNER JOIN ospos_credits AS b On a.credit = b.id WHERE a.id='$id'");
        return $query->row_array();
	}

	public function addTransactions($qty, $denomination, $user_id, $type, $shop)
	{
		$select = $this->db->query("SELECT denomination, wholesale_price, retail_price FROM ospos_credit_denominations WHERE id = '$denomination' and status = 1");
		if($type == 1){
			$amount = $select->row(0)->retail_price;
		}else{
			$amount = $select->row(0)->wholesale_price;
		}
		$den = $select->row(0)->denomination;
		$query = $this->db->query("INSERT into ospos_credit_transactions (user_id, denomination, quantity, type, shop, amount) values ('$user_id', '$denomination', '$qty', '$type', '$shop', '$amount')");
		if($query){
			// return "Inserted Successfully!";
			
			$update = $this->db->query("UPDATE ospos_credits_received_packet_details SET card_qty = card_qty - '$qty' WHERE denomination = '$den' AND shop = '$shop' order by created_at ASC LIMIT 1");
			if($update){
				return "Updated Successfully!";
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function addReceivables($data)
	{
		// $query = $this->db->insert('ospos_credits_received_packets', $data);
		$query = $this->db->insert('ospos_credits_received_packets', $data);
		$insert_id = $this->db->insert_id();
		if($query){
			return $insert_id;
		}else{
			return false;
		}
	}

	public function addTransaction($data)
	{
		$query = $this->db->insert('ospos_credit_transactions', $data);
		if($query){
			return true;
		}else{
			return false;
		}
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		$query = $this->db->update('ospos_credit_transactions', $data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
}
?>
