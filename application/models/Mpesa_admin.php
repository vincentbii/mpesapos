<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* 
*/
class Mpesa_admin extends CI_Model
{
	
	function __construct()
	{
		# code...
	}

	public function get_last_ten_entries($person = '', $to = '', $from = ''){
        $this->db->select('stock_locations.location_name, stock_locations.location_id, mpesa_report.*,people.first_name, people.last_name');
	    $this->db->from('stock_locations');
	    $this->db->where('deleted', 0);
	    $this->db->join('mpesa_report AS mpesa_report', 'mpesa_report.shop = stock_locations.location_id');
	    if($from !== '' && $to !== ''){
	    	$this->db->where('mpesa_report.report_date >= "'.$from.'"');
	    	$this->db->where('mpesa_report.report_date <= "'.$to.'"');
	    }
	    $this->db->join('people AS people', 'people.person_id = mpesa_report.user');
	    if($person !== '' && $person !== 'all'){
	    	$this->db->where('people.person_id', $person);
	    }
	    $q = $this->db->get();
	    if($q->num_rows() > 0)
	    {
	      foreach ($q->result() as $row)
	      {
	        $data[] = $row;
	      }
	      return $data;
	    }
    }

    public function users($value='')
    {
        $this->db->distinct('mpesa_report.user');
    	$this->db->select('mpesa_report.user,people.first_name, people.last_name');
	    $this->db->from('mpesa_report');
	    $this->db->join('people AS people', 'people.person_id = mpesa_report.user');
	    return $this->db->get()->result_array();
    }

    public function alldata_count($value='')
    {
    	$query = $this
                ->db
                ->get('mpesa_report');
    
        return $query->num_rows(); 
    }

    function alldatas($limit,$start,$col,$dir)
    {   
       $query = $this
                ->db
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('mpesa_report');
        
        if($query->num_rows()>0)
        {
            return $query->result(); 
        }
        else
        {
            return null;
        }
        
    }

    function data_search($limit,$start,$search,$col,$dir)
    {
        $query = $this
                ->db
                ->like('id',$search)
                ->or_like('shop',$search)
                ->limit($limit,$start)
                ->order_by($col,$dir)
                ->get('mpesa_report');
        
       
        if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return null;
        }
    }

    function datas_search_count($search)
    {
        $query = $this
                ->db
                ->like('id',$search)
                ->or_like('shop',$search)
                ->get('mpesa_report');
    
        return $query->num_rows();
    }
}