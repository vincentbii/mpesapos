<?php $this->load->view("partial/header"); ?>
<div class="row">
	<div class="col-xs-12">
		<div class="pull-left">
			<h4 style="font-weight: bolder;"><?php echo 'Category:  '.$files[0]->category; ?></h4>
		</div>
		<div class="pull-right">
			<a class="btn btn-info btn-sm" href="<?php echo site_url('libraries'); ?>">Categories</a>
		</div>
		<table class="display" width="100%" id="files">
			<thead>
				<th>#</th>
				<th>File Name</th>
				<th>Description</th>
			</thead>
			<tbody>
				<?php foreach ($files as $key => $value) { ?>
					<tr>
						<td><?php echo $key + 1; ?></td>
						<td><?php
						if($value->allow_links !== null){
							echo '<a alt="Go to URL" target="_blank" href="'.$value->external_link.'">'.$value->external_link.'</a>';
						}else{
							echo '<a alt="Downlod File" href="'.site_url('libraries/downloadFile/').$value->id.'">'.$value->name.'</a>';
						}
						?></td>
						<td><?php echo $value->description; ?></td>
					</tr>
				<?php }	?>
			</tbody>
		</table>
	</div>
</div>
<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript">
	$(document).ready(function() {
		var fTable = $('#files').DataTable({
			paging: false,
			searching: false,

		});
	});
</script>