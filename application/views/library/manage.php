<?php $this->load->view("partial/header"); ?>
<div class="row">
	<div class="col-xs-12">
		<h4>Categories</h4>
		<table class="display" width="100%" id="categories">
			<thead>
				<th>#</th>
				<th>Category Name</th>
				<th>Description</th>
				<th>Action</th>
			</thead>
		</table>
	</div>
</div>
<div style="visibility: hidden;" class="row">
	<div class="col-xs-9">
		<table class="display" width="100%" id="files">
			<thead>
				<th>#</th>
				<th>Category</th>
				<th>File</th>
				<th>Action</th>
			</thead>
		</table>
	</div>
</div>
<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript">
	$(document).ready(function() {
		var cTable = $('#categories').DataTable({
			"ajax": '<?php echo site_url("libraries/categories") ?>',
			"columns":[
				{"data":"id"},
				{"data":"name"},
				{"data":"description"},
				{
					"data": null,
					render: function (data, type, row) {
	            		return "<button id='show_files' class='btn btn-xs btn-info'><i class='fa fa-eye'></i></button>";
	                }
				},
			],
			paging: false,
			searching: false
		});
		$('#categories tbody').on( 'click', '#show_files', function () {
	    	var data = cTable.row( $(this).parents('tr') ).data();
	    	window.location = "<?php echo site_url('libraries/show/') ?>" + data.id;
		} );
		var fTable = $('#files').DataTable({
			"processing": true,
	        "ajax": "<?php echo site_url("libraries/files") ?>",
	        "columns": [
	            { "data": "id" },
	            { "data": "category" },
	            {
	            	"data": "external_link",
	            	render: function (data, type, row) {
	            		if(row.allow_links != null){
	            			return "<a target='_blank' href='"+row.external_link+"'>"+row.external_link+"</a>";
	            		}else{
	            			return row.name;
	            		}
	                }
	            },
	            {
	            	data: 'category_id',
	            	render: function ( data, type, row ) {
				        return '<button value="'+data+'" id="download_file" class="btn btn-xs btn-info"><i class="fa fa-download" aria-hidden="true"></i></button>';
				    }
	            }
	        ]
		});

		$('#files tbody').on( 'click', '#download_file', function () {
	    	save_method = 'download_file';
	    	var data = fTable.row( $(this).parents('tr') ).data();
	    	window.location = "<?php echo site_url('libraries/downloadFile/') ?>" + data.name;
		} );

		


	});
</script>