<?php $this->load->view("partial/header"); ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<?php echo form_open("receivings/getConfirmation", array('id'=>'confirmations', 'class'=>'form-horizontal')); ?>
				<input type="hidden" name="loc_id" value="<?php echo $location['location_id'] ?>">
				<input type="hidden" name="emp_id" value="<?php echo $employee->person_id ?>">
				<table width="100%">
					<tr>
						<td>
							<div class="form-group form-group-sm">
								<?php echo form_label($this->lang->line('reports_stock_location'), 'location', array('class'=>'control-label col-xs-3')); ?>
								<div class='col-xs-8'>
									<?php echo form_input(array('name'=>'location','value'=>$location['location_name'], 'id'=>'location', 'readonly'=>true, 'class'=>'form-control input-sm'));?>
								</div>
							</div>
						</td>
						<td>
							<div class="form-group form-group-sm">
								<?php echo form_label($this->lang->line('reports_employee'), 'location', array('class'=>'control-label col-xs-3')); ?>
								<div class='col-xs-8'>
									<?php echo form_input(array('name'=>'employee','value'=>$employee->first_name.' '.$employee->last_name, 'id'=>'employee', 'readonly'=>true, 'class'=>'form-control input-sm'));?>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="form-group form-group-sm">
								<?php echo form_label($this->lang->line('reports_comment'), 'location', array('class'=>'control-label col-xs-2')); ?>
								<div class='col-xs-9'>
									<?php echo form_textarea(array('name'=>'comment','value'=>'', 'id'=>'employee', 'class'=>'form-control input-sm', 'rows'	=>	5));?>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>
							<div class="form-group form-group-sm">
								<?php echo form_label('Supervisor '. $this->lang->line('reports_signature'), 'location', array('class'=>'control-label col-xs-3')); ?>
								<div class='col-xs-8'>
									<?php echo form_input(array('name'=>'supervisor_name','value'=>'', 'id'=>'employee', 'class'=>'form-control input-sm'));?>
								</div>
							</div>
						</td>
						<td>
							<div class="form-group form-group-sm">
								<?php echo form_label('Shop Attendant '. $this->lang->line('reports_signature'), 'location', array('class'=>'control-label col-xs-3')); ?>
								<div class='col-xs-8'>
									<?php echo form_input(array('name'=>'attendant_name','value'=>'', 'id'=>'employee', 'class'=>'form-control input-sm'));?>
								</div>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="pull-right">
							<button class="btn btn-primary" id="save">Save</button>
						</td>
					</tr>
				</table>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<?php $this->load->view("partial/footer"); ?>

<script type="text/javascript">
$(document).ready(function()
{
	var submit_form = function()
	{
		$(this).ajaxSubmit(
		{
			success:function(response)
			{
				dialog_support.hide();
				table_support.handle_submit('<?php echo site_url('receivings'); ?>', response);
			},
			dataType:'json'
		});
	};

	}
</script>