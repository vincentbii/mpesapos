<?php $this->load->view("partial/header"); ?>
  <ul id="error_message_box" class="error_message_box"></ul>
  <div class="row">
    <div class="col-xs-12">
      <h4>Mpesa Transactions Report</h4>
      <div class="col-md-12 col-md-offset-0">
          <form class="form-inline"  method="post" >
            <div class="form-group">
              <label for="from">From Date: </label>
              <input class="awesome" type="date"  id="from" value="<?php echo date('Y-m-d'); ?>" class="input-sm"  placeholder="From Date" required>
              <label for="to">To Date : </label>
                <input class="awesome" type="date"  id="to" value="<?php echo date('Y-m-d'); ?>" class="input-sm"  placeholder="To Date" required>
              <label for="person">&emsp;System User : </label>
              <select class="awesome" name="person" id="person" required>
                <?php
                echo "<option value='all'> -- All Users -- </option>";
                foreach ($persons as $key => $value) {
                  echo "<option value='".$value['user']."'>".$value['first_name']."</option>";
                }
                ?>
              </select>
                &emsp;&emsp;&emsp;
                <button type="submit" id="search" class="btn btn-xs btn-primary">Search</button>
            </div>
           </form> 
        </div>
      <table class="display" width="100%" id="report">
        <thead>
          <th>Date</th>
          <th>Shop Attendant</th>
          <th>Shop Name</th>
          <th>Target</th>
          <th>Deposits</th>
          <th>Withdrawals</th>
          <th>Opening Float</th>
          <th>Available Cash</th>
          <th>Deficit</th>
          <th>Closing Float</th>
        </thead>
        <tfoot>
          <th colspan="3">Total</th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </tfoot>
      </table>
    </div>
  </div>
<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript">
  $(document).ready(function() {
    var oTable = $('#report').DataTable({
        "ajax": {
            "url": "<?php echo site_url("mpesaadmin/report") ?>",
            "type": "POST",
            "data":function(data) {
              data.from = $('#from').val();
              data.to = $('#to').val();
              data.person = $('#person').val();
                data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
            },
        },
        "dataSrc":"",
        "columns": [
            {"data": null,
              render: function (data, type, row) {
                  var mydate = new Date(row.report_date);
                  var str = moment(mydate).format("YYYY-MM-DD");
                  return str;
              }
            },
            { 
                "data": null,
                render: function (data, type, row) {
                    var person = row.first_name + ' ' + row.last_name;
                    return person;
                }
            },
            { "data": "location_name" },
            { "data": "target" },
            { "data": "deposits" },
            { "data": "withdrawals" },
            { "data": "opening_float" },
            { "data": "available_cash" },
            { "data": "deficit" },
            { 
                "data": null,
                render: function (data, type, row) {
                    var closing_float = +row.deficit + +row.opening_float + +row.available_cash;
                    return closing_float;
                }
            }
        ],
        drawCallback: function () {
          var api = this.api();
          $( api.column(3).footer() ).html(
            api.column( 3 ).data().sum()
          );
          $( api.column(4).footer() ).html(
            api.column( 4 ).data().sum()
          );
          $( api.column(5).footer() ).html(
            api.column( 5 ).data().sum()
          );
          $( api.column(6).footer() ).html(
            api.column( 6 ).data().sum()
          );
          $( api.column(7).footer() ).html(
            api.column( 7 ).data().sum()
          );
          $( api.column(8).footer() ).html(
            api.column( 8 ).data().sum()
          );
          $( api.column(9).footer() ).html(
            api.column( 6 ).data().sum() + api.column( 7 ).data().sum() + api.column( 8 ).data().sum()
          );
        }
    });
     
      $('#search').on( 'click change', function (event) {
          event.preventDefault();

          if($('#from').val()=="")
          {
            $('#from').focus();
          }
          else if($('#to').val()=="")
          {
            $('#to').focus();
          }
          else if($('#person').val()=="")
          {
            $('#person').focus();
          }
          else
          {
            $('#report').DataTable().ajax.reload(); 
          }

        } );

  });
</script>