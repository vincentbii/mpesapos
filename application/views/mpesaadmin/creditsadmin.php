<?php $this->load->view("partial/header"); ?>
<script type="text/javascript">
  function filerB(){
    var url;
    url = "<?php echo site_url('credits_admin/index')?>";
      $.ajax({
            url : url,
            type: "POST",
            data: $('#formfilter').serialize(),
            success: function(data)
            {
               //if success close modal and reload ajax table
               // $('#modal_form').modal('hide');
               // window.location.href = '<?php //echo site_url("credits"); ?>/' + data.id + '/' + data.qty;
              // location.reload();// for reload a page
              alert("YES")
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
        });
    }
</script>
<div class="container">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
	<div class="row">
  <h1>Airtime Management</h1>
	<table id="credits" class="display compact nowrap" style="width:100%">
		<thead>
			<tr>
				<th>#</th>
				<th>Airtime Den</th>
				<th>Packets in Store</th>
        <th>Wholesale Price</th>
        <th>Retail Price</th>
        <th>Buying Price</th>
				<th>Action</th>
			</tr>
		</thead>
	</table>
</div>
<div class="row">
  <h3>Airtime Transactions Detailed Report</h3>
  <div>
    <div class="well">
            <div class="">
                <div class="">
                <div class="row">
                  <div class="col-md-12 col-md-offset-0">
                    <form class="form-inline"  method="post" >
                      <div class="form-group">
                        <label for="fromdate">From Date: </label>
                        <input class="awesome" type="date"  id="datepicker1" value="<?php echo date('Y-m-d'); ?>" class="input-sm"  placeholder="FROM DATE" required>
                        <label for="todate">To Date : </label>
                          <input class="awesome" type="date"  id="datepicker2" value="<?php echo date('Y-m-d'); ?>" class="input-sm"  placeholder="TO DATE" required>
                        <label for="person">&emsp;System User : </label>
                        <select class="awesome" name="person" id="person" required>
                          <?php
                          echo "<option value='all'> -- All Users -- </option>";
                          foreach ($persons as $key => $value) {
                            echo "<option value='".$value['user_id']."'>".$value['first_name']."</option>";
                          }
                          ?>
                        </select>
                        <label for="shop">&emsp;Shop : </label>
                        <select class="awesome" name="shop" id="shop" required>
                          <?php
                          echo "<option value='all'> -- All Shops -- </option>";
                          foreach ($shops as $ky => $valu) {
                            echo "<option value='".$valu['location_id']."'>".$valu['location_name']."</option>";
                          }
                          ?>
                        </select>
                        <label for="ttype">&emsp;Transaction Type : </label>
                        <select class="awesome" name="ttype" id="ttype" required>
                          <option value="all"> -- All -- </option>
                          <option value="1">Retail</option>
                          <option value="2">Wholesale</option>
                        </select>
                          &emsp;&emsp;&emsp;<button style="background-color: rgb(124,77,255);color: #fff" type="submit" id="search"  class="btn btn-xs btn-default">SEARCH</button>
                      </div>
                     </form> 
                  </div>
                </div>
                </div>
            </div>
        </div>
  <table id="allTransactions" class="display">
    <thead>
      <tr>
        <th>#</th>
        <th>Bamba</th>
        <th>Transaction Type</th>
        <th>Selling Price</th>
        <th>Transaction Date</th>
        <th>System User</th>
      </tr>
    </thead>
  </table>
</div>
<div class="row">
  <h3>Airtime Transactions Summary Report</h3>
  <div class="well">
            <div class="">
                <div class="">
                <div class="row">
                  <div class="col-md-12 col-md-offset-0">
                    <form class="form-inline"  method="post" >
                      <div class="form-group">
                        <label for="fromdate1">From Date: </label>
                        <input class="awesome" type="date"  id="datepicker11" value="<?php echo date('Y-m-d'); ?>" class="input-sm"  placeholder="From Date" required>
                        <label for="todate">To Date : </label>
                          <input class="awesome" type="date"  id="datepicker21" value="<?php echo date('Y-m-d'); ?>" class="input-sm"  placeholder="To Date" required>
                        <label for="person1">&emsp;System User : </label>
                        <select class="awesome" name="person1" id="person1" required>
                          <?php
                          echo "<option value='all'> -- All Users -- </option>";
                          foreach ($persons as $key => $value) {
                            echo "<option value='".$value['user_id']."'>".$value['first_name']."</option>";
                          }
                          ?>
                        </select>
                        <label for="shop1">&emsp;Shop : </label>
                        <select class="awesome" name="shop1" id="shop1" required>
                          <?php
                          echo "<option value='all'> -- All Shops -- </option>";
                          foreach ($shops as $ky => $valu) {
                            echo "<option value='".$valu['location_id']."'>".$valu['location_name']."</option>";
                          }
                          ?>
                        </select>
                        <label for="ttype1">&emsp;Transaction Type : </label>
                        <select class="awesome" name="ttype1" id="ttype1" required>
                          <option value="all"> -- All -- </option>
                          <option value="1">Retail</option>
                          <option value="2">Wholesale</option>
                        </select>
                          &emsp;&emsp;&emsp;<button style="" type="submit" id="search1"  class="btn btn-xs btn-info">Search</button>
                      </div>
                     </form> 
                  </div>
                </div>
                </div>
            </div>
        </div>
  <table id="summaryTransactions" class="display">
    <thead>
      <tr>
        <th>Bamba</th>
        <th>Total Cards Sold</th>
        <th>Total Transaction Amount</th>
        <th>WholeSale Transactions</th>
        <th>Wholesale Transaction Amount</th>
        <th>Total WholeSale Buying Price</th>
        <th>Turnover</th>
        <th>Retail Transactions</th>
        <th>Retail Transaction Amount</th>
        <th>Total Retail Buying Price</th>
        <th>Turnover</th>
        <th>Qty Issued</th>
        <th>Qty Remaining</th>
      </tr>
    </thead>
  </table>
</div>

<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
    
	    var table = $('#credits').DataTable( {
        "dom": 'Bfrtip',
          "buttons": [
              {
                  "text": 'Issue Items',
                  "className": 'Issue_items',
                  action: function ( e, dt, node, config ) {
                    save_method = "issue";
                    $.ajax({  
                          url:"<?php echo site_url("mpesa/shops") ?>", 
                          method:"GET",  
                          success:function(data, array){  
                              $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                              $('.modal-title').text('Issue Packets');
                              $('#Bamba').hide();
                              $('#btnSave').hide();
                              $('#btnIssue').show();
                              $('#modal_form').modal('show');
                              $('select[name="shop"]').empty();
                              $.each(data.shops, function(key, value) {
                                  $('select[name="shop"]').append('<option value="'+ value.location_id +'">'+ value.location_name +'</option>');
                              });
                              $('select[name="user"]').empty();
                              $.each(data.users, function(key, value) {
                                  $('select[name="user"]').append('<option value="'+ value.person_id +'">'+ value.username +'</option>');
                              });
                          }  
                     });
                  }
              }
          ],
	        "ajax": "<?php echo site_url("credits_admin/getCredit") ?>",
	        "columns": [
	            { "data": "id" },
	            { "data": "denomination" },
              { "data": "qty" },
	            { "data": "wprice" },
	            { "data": "rprice" },
	            { "data": "bprice" },
	            { "data": {
	            	"targets": -1,
	            	"data": "id",
	            	"defaultContent": "<button class='btn btn-xs btn-success'><i class='glyphicon glyphicon-edit'></i></button>"
	            }}
	        ],
	        "columnDefs": [ {
	            	"targets": -1,
	            	"data": "id",
	            	"defaultContent": "<button id='add_item' class='btn btn-xs btn-success'><i class='glyphicon glyphicon-plus'></i></button><button id='edit_item' class='btn btn-primary btn-xs'><i class='glyphicon glyphicon-edit'></i></button>"
	        } ]
	    } );

      var table1 = $('#allTransactions').DataTable( {
          "dom": 'Bfrtip',
          "buttons": [
              'copy', 'csv', 'excel', 'pdf', 'print'
          ],
          "ajax": {
            "url":"<?php echo site_url("credits_admin/getaltransactions") ?>",
            "type": "POST",
            "data":function(data) {
              data.from = $('#datepicker1').val();
              data.to = $('#datepicker2').val();
              data.person = $('#person').val();
              data.ttype = $('#ttype').val();
              data.shop = $('#shop').val();
                data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
            },
          },
          "columns": [
              { "data": "id" },
              { "data": "denomination" },
              { "data": "credit_type"},
              { "data": "amount"},
              { "data": "datetime"},
              { 
                "data": null, 
                render: function (data, type, row) {
                            var details = row.first_name + " " + row.last_name;
                            return details;
                }
            }
          ]
      } );

      $('#search').on( 'click change', function (event) {
          event.preventDefault();

          if($('#datepicker1').val()=="")
          {
            $('#datepicker1').focus();
          }
          else if($('#datepicker2').val()=="")
          {
            $('#datepicker2').focus();
          }
          else if($('#person').val()=="")
          {
            $('#person').focus();
          }
          else if($('#ttype').val()=="")
          {
            $('#ttype').focus();
          }
          else if($('#shop').val()=="")
          {
            $('#shop').focus();
          }
          else
          {
            $('#allTransactions').DataTable().ajax.reload(); 
          }

        } );

      var table2 = $('#summaryTransactions').DataTable( {
          "dom": 'Bfrtip',
          "buttons": [
              {
                extend: 'copy',
                text: 'Copy',
                className: 'copyButton'
              }, 'csv', 'excel', 'pdf', 'print'
          ],
          "ajax": {
            "url":"<?php echo site_url("credits_admin/getTransactionsSummary") ?>",
            "type": "POST",
            "data":function(data) {
              data.from = $('#datepicker11').val();
              data.to = $('#datepicker21').val();
              data.person = $('#person1').val();
              data.ttype = $('#ttype1').val();
              data.shop = $('#shop1').val();
                data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
            },
          },
          "columns": [
              { "data": "denomination" },
              { "data": "ttransactions" },
              { "data": "ttamount"},
              { "data": "wTrans"},
              { "data": "wTransA"},
              { "data": "TotalWBPrice"},
              { "data": "wTO"},
              { "data": "rTrans"},
              { "data": "rTransA"},
              { "data": "TotalRBPrice"},
              { "data": "wTOR"},
              { "data": "issued" },
              { "data": "Nissued" }
          ]
      } );


      $('#search1').on( 'click change', function (event) {
          event.preventDefault();

          if($('#datepicker11').val()=="")
          {
            $('#datepicker11').focus();
          }
          else if($('#datepicker21').val()=="")
          {
            $('#datepicker21').focus();
          }
          else if($('#person1').val()=="")
          {
            $('#person1').focus();
          }
          else if($('#ttype1').val()=="")
          {
            $('#ttype1').focus();
          }
          else if($('#shop1').val()=="")
          {
            $('#shop1').focus();
          }
          else
          {
            $('#summaryTransactions').DataTable().ajax.reload(); 
          }

        } );

	    $('#credits tbody').on( 'click', '#add_item', function () {
	    	save_method = 'add';
	    	$('#form')[0].reset();
        	var data = table.row( $(this).parents('tr') ).data();
        	// alert( data['id'] +"'s salary is: "+ data['selling_price'] );
          $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
          $('[name="denomination"]').val(data.denomination);
          $('[name="id"]').val(data.id);
          $('.modal-title').text('Add packets');
          $('#btnIssue').hide();
          $('#btnSave').show();
          $('#shop').hide();
          $('#user').hide();

    	} );
      $('#credits tbody').on( 'click', '#edit_item', function () {
        var data = table.row( $(this).parents('tr') ).data();
        save_method = 'edit';
               $('#modal_form1').modal('show');
               $('[name="denomination"]').val(data.denomination);
               $('[name="id"]').val(data.id);
               $('[name="wprice"]').val(data.wprice);
               $('[name="bprice"]').val(data.bprice);
               $('[name="rprice"]').val(data.rprice);
      } );
	} );



	function save(){
        var url;
          if(save_method == 'add')
          {
              url = "<?php echo site_url('Credits_admin/addReceivables')?>";
          }
          else if(save_method == "issue"){
            url = "<?php echo site_url('Credits_admin/saveIssued')?>";
          }
          else
          {
            url = "<?php echo site_url('credits_admin/editDenomination')?>";
          }
          

          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               // $('#modal_form').modal('hide');
               // window.location.href = '<?php //echo site_url("credits"); ?>/' + data.id + '/' + data.qty;
               alert("Added Successfully")
              location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
        });
        
    }

    function edit(){
        var url;
            url = "<?php echo site_url('credits_admin/editDenomination')?>";
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form1').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               // window.location.href = '<?php echo site_url("credits"); ?>/' + data.id + '/' + data.qty;
               alert("Updated Successfully")
              location.reload();// for reload a page
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
        
    }


  function issue(){
        var url;
            url = "<?php echo site_url('Credits_admin/saveIssued')?>";
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               // window.location.href = '<?php echo site_url("credits"); ?>/' + data.id + '/' + data.qty;
               alert("Updated Successfully!")
              location.reload();// for reload a page
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
        
    }    

</script>
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Receive Items</h3>
      </div>
      <div class="modal-body form">
        <?php echo form_open('#', array('id'=>'form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
            <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>">

            <input type="hidden" name="id">
          <div class="form-body">
        <div class="form-group form-group-sm" id="Bamba">
            <?php echo form_label('Bamba', 'denomination', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'denomination',
                        'id'=>'denomination',
                        'type' => 'text',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>

        <div id="shop" class="form-group form-group-sm">
            <?php echo form_label('Shop', 'shop', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <select name="shop" class="form-control" style="width:350px">
                </select>
            </div>
        </div>

        <div id="user" class="form-group form-group-sm">
            <?php echo form_label('Shop Attendant', 'user', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <select name="user" class="form-control" style="width:350px">
                </select>
            </div>
        </div>

        <fieldset>
          <legend style="font-size: 12px;">Serial Number(s) Range</legend>
          <div class="form-group form-group-sm">
            <?php echo form_label('From', 'min', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'min',
                        'id'=>'min',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>

        <div class="form-group form-group-sm">
            <?php echo form_label('To', 'max', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'max',
                        'id'=>'max',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>
        </fieldset>
 
          </div>
        <?php echo form_close(); ?>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" id="btnIssue" onclick="issue()" class="btn btn-primary">Issue</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" id="modal_form1" role="dialog">

  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Edit Card Details</h3>
      </div>
      <div class="modal-body form">
        <?php echo form_open('#', array('id'=>'form1', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
            <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>">

            <input type="hidden" name="id">
          <div class="form-body">
        <div class="form-group form-group-sm">
            <?php echo form_label('Denomination', 'denomination', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'denomination',
                        'id'=>'denomination',
                        'type' => 'text',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>

        <div class="form-group form-group-sm">
            <?php echo form_label('Buying Price', 'bprice', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'bprice',
                        'id'=>'bprice',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>

        <div class="form-group form-group-sm">
            <?php echo form_label('Wholesale Price', 'wprice', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'wprice',
                        'id'=>'wprice',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>

        <div class="form-group form-group-sm">
            <?php echo form_label('Retail Price', 'rprice', array('class'=>'required control-label col-xs-3')); ?>
            <div class='col-xs-8'>
                <?php echo form_input(array(
                        'name'=>'rprice',
                        'id'=>'rprice',
                        'type' => 'number',
                        'class'=>'form-control input-sm')
                        );?>
            </div>
        </div>
 
          </div>
        <?php echo form_close(); ?>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="edit()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->