<?php $this->load->view("partial/header"); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<div class="row">
	<h2>Issues Management</h2>
	<table id="float" class="cell-border compact stripe" style="width:100%">
		<thead>
			<tr>
				<th>#</th>
				<th>Serial Number</th>
				<th>Denomination</th>
			</tr>
		</thead>
	</table>
</div>


<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    var table = $('#float').DataTable( {
	    	"dom": 'Bfrtip',
	          "buttons": [
	              {
	                  "text": 'Issue Items',
	                  "className": 'issueItems',
	                  action: function ( e, dt, node, config ) {
	                      // window.location.href = '<?php echo site_url("credits"); ?>/' + data.id + '/' + data.qty;
	                  }
	              }
	          ],
	        "ajax": {
	        	url : "<?php echo site_url("credits/getReceivables") ?>",
            	type : 'GET'
	        },
	        "columns": [
	        	{ "data" : 'id'},
	        	{ "data" : 'serial_number'},
	        	{ "data" : 'denomination'}
	        ],
	        "columnDefs": [ {
	          "targets": -1,
	          "data": null,
	          "defaultContent": 
	             '<button class="btn btn-xs btn-info btn-view" type="button">Edit</button>'
	        } ]
	    } );

	    $('#float tbody').on( 'click', 'tr', function () {
	        $(this).toggleClass('selected');
	    } );

	    $('#button').click( function () {
	        alert( table.rows('.selected').data().length +' row(s) selected' );
	    } );

	} );

	

</script>
