<?php $this->load->view("partial/header"); ?>
<div class="container form">
	<?php echo form_open('credits/save', array('id'=>'form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
            <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>">
          <div class="form-body">
            <input type="hidden" name="id" value="<?php  echo $id; ?>">

           <?php for ($i=0; $i < $qty; $i++) { ?>
				<div class="form-group form-group-sm">
					<?php echo form_label('Serial No.:', 'serial_no', array('class'=>'control-label col-xs-3')); ?>
					<div class='col-xs-1'>
						<?php echo form_input(array(
								'name'=>'serial_no[]',
								'id'=>'serial_no[]',
								'placeholder' => 'Serial Number'
							)
								);?>
					</div>
				</div>
			<?php } ?>

			<div class="form-group form-group-sm">
				<?php
						$databtnr = array(
					        'name'          => 'button',
					        'id'            => 'button',
					        'value'         => 'true',
					        'type'          => 'reset',
					        'content'       => 'Reset',
					        'class'			=> 'btn btn-primary btn-xs'
						);

						echo form_button($databtnr);

						$databtns = array(
					        'name'          => 'button',
					        'id'            => 'button',
					        'value'         => 'true',
					        'type'          => 'submit',
					        'content'       => 'Submit',
					        'class'			=> 'btn btn-info btn-xs'
						);

						echo form_button($databtns);
					?>
			</div>
 
          </div>
        <?php echo form_close(); ?>
</div>
<script type="text/javascript">
	function Save(){
		url = "<?php echo site_url('credits/save')?>";
		$.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               // $('#modal_form').modal('hide');
               alert(data)
              // location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
        });
	}
</script>
<?php $this->load->view("partial/footer"); ?>