<?php $this->load->view("partial/header"); ?>
<ul id="error_message_box" class="error_message_box"><?php echo $error;?></ul>
<div class="row">
		<h4>Library Files</h4>
		<table class="display" id="files" width="100%">
			<thead>
				<th>#</th>
				<th>Category</th>
				<th>File Name</th>
				<th>Link Description</th>
				<th>Action</th>
			</thead>
		</table>
	</div>
	<div class="row">
		<h4>Library Categories</h4>
		<table class="display" width="100%" id="categories">
			<thead>
				<th>#</th>
				<th>Category Name</th>
				<th>Description</th>
				<th>Action</th>
			</thead>
		</table>
</div>

<?php $this->load->view("partial/footer"); ?>
<script type="text/javascript">
	$(document).ready(function() {
		var fTable = $('#files').DataTable({
			"processing": true,
	        "ajax": "<?php echo site_url("librarian/files") ?>",
	        "columns": [
	            { "data": "id" },
	            { "data": "category" },
	            {
	            	"data": null,
	            	render: function (data, type, row) {
	            		if(row.allow_links != null){
	            			return "<a target='_blank' href='"+row.external_link+"'>"+row.external_link+"</a>";
	            		}else{
	            			return row.name;
	            		}
	                }
	            },
	            { "data": "description" },
	            {
	            	data: null,
	            	render: function ( data, type, row ) {
	            		var editf;
	            		if(row.allow_links == null){
	            			editf = '<button value="'+data+'" id="download_file" class="btn btn-xs btn-info"><i class="fa fa-download" aria-hidden="true"></i></button> ';
	            		}else{
	            			editf = '<button id="edit_file" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>';
	            		}
				        return editf+' <button id="delete_file" value="'+data+'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>';
				    }
	            }
	        ],
	        paging: false,
	        searching: false
		});
	    var cTable = $('#categories').DataTable( {
	    	"dom": 'Bfrtip',
	          "buttons": [
	              {
	                  "text": 'Add Category',
	                  "className": 'add_category',
	                  'class': 'btn btn-success btn-xs',
	                  attr:{
	                  	id:"add_category"
	                  }
	              }
	          ],
	        "processing": true,
	        searching: false,
	        paging: false,
	        "ajax": "<?php echo site_url("librarian/categories") ?>",
	        "columns": [
	            { "data": "id" },
	            { "data": "name" },
	            { "data": "description"},
	            {
	            	data: 'id',
	            	render: function ( data, type, row ) {
				        return '<button id="add_file" class="btn btn-xs btn-success">Add File</button> <button id="add_link" class="btn btn-xs btn-info">Add Link</button> | <button value="'+data+'" id="edit_category" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button> | <button id="delete_category" value="'+data+'" class="btn btn-xs btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>';
				    }
	            }
	        ]
	    } );
      
	    $('#add_category').on( 'click', function () {
	    	save_method = 'add';
	    	$('#form')[0].reset();
	          $('#modal_form').modal('show');
	          $('.modal-title').text('Add Category');
	          $('#btnSave').show();
	          $('#upload').hide();
	          $('#external_link').hide();
            $('#external_link_description').hide();
	          $('#allow_links').hide();
            $('#inner_cancel').hide();
	          $('#file').hide();
	          $('#name').show();
	          $('#category').hide();
    	} );

	    $('#categories tbody').on( 'click', '#delete_category', function () {
	    	save_method = 'delete';
	    	var data = cTable.row( $(this).parents('tr') ).data();
	    	var result = confirm("Want to delete "+data.name+" Category?");
			if (result) {
			    $.ajax({
			    url: "<?php echo site_url('librarian/destroy/')?>"+data.id,
			    type: 'DELETE',
			    success: function(result) {
			        // Do something with the result
			        alert(result)
			        location.reload();
			    },
			    error: function(jqXHR, textStatus, errorThrown){
			    	alert("Could not be deleted. Remove dependent files")
			    }
			});
			}

    	} );

    	$('#files tbody').on( 'click', '#delete_file', function () {
	    	save_method = 'delete';
	    	var data = fTable.row( $(this).parents('tr') ).data();
	    	var result = confirm("Want to delete?");
			if (result) {
			    $.ajax({
			    url: "<?php echo site_url('librarian/destroyFile/')?>"+data.id,
			    type: 'DELETE',
			    success: function(result) {
			        // Do something with the result
			        alert(result)
			        location.reload();
			    }
			});
			}

    	} );

    	$('#categories tbody').on( 'click', '#add_file', function () {
	    	save_method = 'add_file';
	    	var data = cTable.row( $(this).parents('tr') ).data();
	    	$.ajax({  
              url:"<?php echo site_url('librarian/categories/') ?>"+data.id, 
              method:"GET",  
              success:function(result){  
                  $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                  $('.modal-title').text('Add File');
                  $('#btnSave').hide();
                  $('#name').hide();
                  $('#modal-footer').hide();
                  $('#category_description').hide();
                  $('#external_link').hide();
                  $('#external_link_description').hide();
                  $('select[name="category"]').empty();
                  $.each(result.data, function(key, value) {
                      $('select[name="category"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
                  });
              }  
         });

    	} );

    	$('#categories tbody').on( 'click', '#add_link', function () {
	    	save_method = 'add_link';
	    	var data = cTable.row( $(this).parents('tr') ).data();
	    	$.ajax({  
              url:"<?php echo site_url('librarian/categories/') ?>"+data.id, 
              method:"GET",  
              success:function(result){  
                  $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                  $('.modal-title').text('Add Link');
                  $('#upload').hide();
                  $('#category_description').hide();
                  $('#external_link').hide();
                  $('#external_link_description').hide();
                  $('#file').hide();
                  $('#name').hide();
                  $('#inner_cancel').hide();
                  $('select[name="category"]').empty();
                  $.each(result.data, function(key, value) {
                      $('select[name="category"]').append('<option value="'+ value.id +'">'+ value.name +'</option>');
                  });
              }  
         });

    	} );

    	$('#files tbody').on( 'click', '#download_file', function () {
	    	save_method = 'download_file';
	    	var data = fTable.row( $(this).parents('tr') ).data();
	    	window.location = "<?php echo site_url('librarian/downloadFile/') ?>" + data.name;
    	} );

    	$('#categories tbody').on( 'click', '#edit_category', function () {
	    	save_method = 'edit';
	    	var data = cTable.row( $(this).parents('tr') ).data();
	    	$('#form')[0].reset();
	          $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	          $('[name="name"]').val(data.name);
	          $('[name="id"]').val(data.id);$('[name="category_description"]').val(data.description);
	          $('#upload').hide();
	          $('#file').hide();
	          $('#inner_cancel').hide();
	          $('#external_link').hide();
            $('#external_link_description').hide();
	          $('#allow_links').hide();
	          $('.modal-title').text('Edit Category');
	          $('#btnSave').show();
	          $('#category').hide();
	          $('#file').hide();

    	} );

    	$('#files tbody').on( 'click', '#edit_file', function () {
	    	save_method = 'edit_file';
	    	var data = fTable.row( $(this).parents('tr') ).data();
	    	$.ajax({  
              url:"<?php echo site_url('librarian/categories/') ?>", 
              method:"GET",  
              success:function(result){  
                  $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                  $('.modal-title').text('Edit File');
                  $('#name').hide();
                  $('#upload').hide();
                  $('#file').hide();
                  $('#category_description').hide();
                  $('#inner_cancel').hide();
                  $('[name="id"]').val(data.id);
                  $('[name="link_description"]').val(data.description);
                  $('[name="external_link"]').val(data.external_link);
                  $('[name="allow_links"]').val(data.allow_links);
                  $('select[name="category"]').empty();
                  $.each(result.data, function(key, value) {
                      $('select[name="category"]').append('<option selected="'+data.category_id+'" value="'+ value.id +'">'+ value.name +'</option>');
                  });
              }  
         });

    	} );

	} );

	function save(){
		var url;
      if(save_method == 'add')
      {
          url = "<?php echo site_url('librarian/storeCategory')?>";
      }
      else if(save_method == "edit"){
        url = "<?php echo site_url('librarian/update')?>";
      }
      else if(save_method == "delete")
      {
        url = "<?php echo site_url('librarian/delete')?>";
      }
      else if(save_method == "edit_file"){
      	url = "<?php echo site_url('librarian/updateFile')?>";
      }else if(save_method == "add_file"){
      	url = "<?php echo site_url('librarian/addFile')?>";
      }else if(save_method == "add_link"){
      	url = "<?php echo site_url('librarian/addLink')?>";
      }
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               alert(data)
              	location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
        });
        
    }
</script>
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title"></h3>
      </div>
      <div class="modal-body form">
        <?php echo form_open_multipart('librarian/addFile', array('id'=>'form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
            <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>">
            <div class="form-group" id="name">
            	<div class="col-xs-3">
            		<label>Category Name</label>
            	</div>
            	<div class="col-xs-9">
            		<input class="form-control input-sm" placeholder="Name" type="text" name="name">
            	</div>
            </div>
            <input type="hidden" name="id">
            <br>
            <div class="form-group" id="external_link">
            	<div class="col-xs-3">
            		<label>External Link</label>
            	</div>
            	<div class="col-xs-9">
            		<input type="text" name="external_link" placeholder="External Link" class="form-control input-sm">
            	</div>
            </div>

            <div class="form-group" id="external_link_description">
            	<div class="col-xs-3">
            		<label>Description</label>
            	</div>
            	<div class="col-xs-9">
            		<textarea name="link_description" class="form-control"></textarea>
            	</div>
            </div>
            <div class="form-group" id="category">
            	<div class="col-xs-3">
            		<label>Choose Category</label>
            	</div>
            	<div class="col-xs-9">
            		<select class="form-control input-sm" name="category" class="" style="width:350px">
                </select>
            	</div>
            </div>
            <div class="form-group" id="category_description">
            	<div class="col-xs-3">
            		<label>Category Description</label>
            	</div>
            	<div class="col-xs-9">
            		<textarea class="form-control" rows="5" cols="10" name="category_description"></textarea>
            	</div>
            </div>
            
            <div class="form-group" id="file">
            	<div class="col-xs-3"></div>
            	<div class="col-xs-9">
            		<input class="form-control-file" type="file" name="userfile" size="20" />
            	</div>
            </div>
            <div class="form-group" id="allow_links">
            	<div class="col-xs-3"></div>
            	<div class="col-xs-9">
            		<input type="checkbox" value="true" name="allow_links">Check if external link
            	</div>
            </div>
            <input type="hidden" name="wxyz">
            <input id="upload" class="btn btn-primary" type="submit" value="upload" />
            <button id="inner_cancel" class="pull-right btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
        <?php echo form_close(); ?>
          </div>
          <div id="modal-footer" class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->