<div id="add_Modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Add Service</h4>  
                </div>  
                <div class="modal-body">  
                     <form method="post" id="insert_form">
                          <label>Enter Service Code</label>  
                          <input type="text" name="service_code" id="service_code" class="form-control" />  
                          <br /> 
                          <label>Enter Descripton</label>  
                          <input type="text" name="item_description" id="item_description" class="form-control" />  
                          <br />  
                          <label>Enter Unit Price</label>  
                          <input type="number" name="unit_price" id="unit_price" class="form-control" />  
                          <br /> 
                          <label>Enter Quantity</label>  
                          <input type="number" name="quantity" id="quantity" class="form-control" />  
                          <br />  
                          <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />  
                     </form>  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>