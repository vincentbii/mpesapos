<?php $this->load->view("partial/header"); ?>
<div class="row">
    <div class="col-xs-12">
        <h4>Transactions Report</h4>
        <table class="display" width="100%" id="report">
            <thead>
                <th>Shop</th>
                <th>Target</th>
                <th>Withdrawals</th>
                <th>Deposits</th>
                <th>Deficit</th>
                <th>Opening float</th>
                <th>Available Cash</th>
                <th>Closing Float</th>
                <th>Action</th>
            </thead>
        </table>
    </div>
</div>
<?php $this->load->view("partial/footer"); ?>
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title"></h3>
      </div>
      <div class="modal-body form">
        <?php echo form_open('', array('id'=>'form', 'enctype'=>'multipart/form-data', 'class'=>'form-horizontal')); ?>
            <input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>">
            <div class="form-group">
                <div class="col-xs-3">
                    <label>Target</label>
                </div>
                <div class="col-xs-9">
                    <input class="form-control input-sm" id="target" placeholder="Target" type="text" name="target">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label>Withdrawals</label>
                </div>
                <div class="col-xs-9">
                    <input class="form-control input-sm" id="withdrawals" placeholder="Withdrawals" type="text" name="withdrawals">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label>Deposits</label>
                </div>
                <div class="col-xs-9">
                    <input class="form-control input-sm" id="deposits" placeholder="Deposits" type="text" name="deposits">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label>Opening Float</label>
                </div>
                <div class="col-xs-9">
                    <input class="form-control input-sm" id="opening_float" placeholder="Opening Float" type="text" name="opening_float">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label>Available Cash</label>
                </div>
                <div class="col-xs-9">
                    <input class="form-control input-sm" id="available_cash" placeholder="Available Cash" type="text" name="available_cash">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-3">
                    <label>Deficit</label>
                </div>
                <div class="col-xs-9">
                    <input class="form-control input-sm" id="deficit" placeholder="Deficit" type="text" name="deficit">
                </div>
            </div>
            
            <input type="hidden" name="location_id">
            <input type="hidden" name="unknown">
            
        <?php echo form_close(); ?>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function() {
        var rTable = $('#report').DataTable({
            "ajax": "<?php echo site_url("mpesa/report") ?>",
            "dataSrc":"",
            "columns": [
                { "data": "location_name" },
                { "data": "target" },
                { "data": "withdrawals" },
                { "data": "deposits" },
                { "data": "deficit" },
                { "data": "opening_float" },
                { "data": "available_cash" },

                { 
                    "data": null,
                    render: function (data, type, row) {
                        var closing_float = +row.deficit + +row.opening_float + +row.available_cash;
                        return closing_float;
                    }
                },
                { 
                    "data": "location_id",
                    render: function (data, type, row) {
                        return '<button id="edit_report" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>';
                    }
                }
            ],
            searching: false,
            paging: false,
            lengthChange: false
        });

        $('#report tbody').on( 'click', '#edit_report', function () {
            save_method = 'edit';
            var data = rTable.row( $(this).parents('tr') ).data();
                  $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                  $('.modal-title').text('Edit Mpesa Report');
                  $('[name="target"]').val(data.target);
                  $('[name="withdrawals"]').val(data.withdrawals);
                  $('[name="deficit"]').val(data.deficit);
                  $('[name="deposits"]').val(data.deposits);
                  $('[name="opening_float"]').val(data.opening_float);
                  $('[name="available_cash"]').val(data.available_cash);
                  $('[name="location_id"]').val(data.location_id);
        });
    });

    function save(){
        var url;
        url = "<?php echo site_url('mpesa/edit')?>";
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //if success close modal and reload ajax table
               $('#modal_form').modal('hide');
               alert(data)
                location.reload();// for reload a page
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);
            }
        });
    }
</script>

