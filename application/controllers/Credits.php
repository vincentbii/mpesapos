<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once("Secure_Controller.php");

class Credits extends Secure_Controller
{
	public function __construct()
	{
		parent::__construct('credits');
		$this->load->model('CreditTransactions');
		$this->load->model('Credit');
		$this->load->model('Stock_location');
	}

	public function index()
	{
		
		$this->load->view('mpesa/credittransactions');
	}

	public function indexRec()
	{
		$this->load->view('mpesaadmin/issueItems');
	}

	public function update($id, $data)
	{
		$data = array(
	          'amount' => $this->input->post('amount')
	      );
	    $update = $this->CreditTransactions->update($this->input->post('id'), $data);
	    echo json_encode("Updated Successfully!");
	}

	public function edit($id)
	{
		$data = $this->CreditTransactions->edit($id);
    	echo json_encode($data);
	}

	public function addTransactions()
	{
		// $data = array(
	          $quantity = $this->input->post('quantity');
	          $denomination = $this->input->post('denomination');
	          $user_id = $this->Employee->get_logged_in_employee_info()->person_id;
	          $type = $this->input->post('ttype');
	          $shop = $this->input->post('shop');
	      // );

		$insert = $this->CreditTransactions->addTransactions($quantity, $denomination, $user_id, $type, $shop);
	    echo json_encode($insert);
	}

	public function getTransactions()
	{
		$credits['data'] = $this->CreditTransactions->getCreditTransactions();
		header('Content-Type: application/json');
	    echo json_encode($credits);
	}

	public function saveIssued()
	{
		$selectedCredit = $this->input->post('arraySelected');
		$shop = $this->input->post('shop');
		$data = json_decode($selectedCredit);

		$selected = explode(",", $selectedCredit);
		foreach ($data as $key => $value) {
			$this->Credit->saveIssued($shop, $value->id, $value->serial_number, $value->denomination);			
		}
		echo json_encode("Updated");
	}

	public function creDetails()
	{
		$data['data'] = $this->Credit->getCreditDetails();
		header('Content-Type: application/json');
	    echo json_encode($data);
	}

	public function getCredits()
	{
		$data = $this->Credit->getCredit();
		header('Content-Type: application/json');
	    echo json_encode($data);
	}

	public function getDenominations()
	{
		$data['denominations'] = $this->Credit->getDenominations();
		// $data['locations'] = $this->MpesaShops->getAll();
		$data['shops']	= $this->Stock_location->get_undeleted();
		header('Content-Type: application/json');
	    echo json_encode($data);
	}

	public function getReceivables()
	{
		$credits['data'] = $this->Credit->getReceivables();
		header('Content-Type: application/json');
	    echo json_encode($credits);
	}

	public function rec_details($id, $qty)
	{
		$data['id'] = $id;
		$data['qty'] = $qty;
		$this->load->view('credits/rec_details', $data);
	}

	public function save()
	{
		$snumbers = $this->input->post('serial_no');
		$x = '';
		foreach ($snumbers as $k => $value[]) {
			$sno = $value[$k];
			$id = $this->input->post('id');
			$this->Credit->saveRecDetails($sno, $id);
		}
		$this->load->view('mpesaadmin/creditsadmin');
	}

}
?>
