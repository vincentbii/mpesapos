<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once("Secure_Controller.php");

class Credits_admin extends Secure_Controller
{
	public function __construct()
	{
		parent::__construct('credits');
		$this->load->model('Credit');
		$this->load->model('Stock_location');
		$this->load->model('CreditTransactions');
	}

	public function addReceivables()
	{
		$from = $this->input->post('min');
		$to = $this->input->post('max');
		
		$qty = ($to - $from) + 1;
		$data = array(
	          'qty_packets' => $qty,
	          'denomination' => $this->input->post('id'),
	          'user' => $this->Employee->get_logged_in_employee_info()->person_id
	      );

		$insert = $this->Credit->addBamba($data);

		for ($i=$from; $i <= $to; $i++) { 
			$iii = $this->Credit->saveRecDetails($i, $insert);
		}
		// $insertValues = [
		// 	'id' => $insert,
		// 	'qty' => $this->input->post('quantity')
		// ];
	    echo json_encode("Inserted Successfully!");
	}

	public function index()
	{
		$transactions['persons'] = $this->Credit->getPersons();
		$transactions['shops'] = $this->Credit->getShops();
		// $transactions['locations'] = $this->Credit->getShops();
		$this->load->view('mpesaadmin/creditsadmin', $transactions);
	}

	public function issueDetails()
	{
		$this->load->view('credits/issueDetails');
	}

	public function editDenomination()
	{
		$data = array(
	          'buying_price' => $this->input->post('bprice'),
	          'wholesale_price' => $this->input->post('wprice'),
	          'retail_price' => $this->input->post('rprice')
	      );
		$id = $this->input->post('id');
		$update = $this->Credit->updateDenomination($id,$data);
		// $denomination['data'] = $this->input->post('data');
		header('Content-Type: application/json');
	    echo json_encode($update);
	    // echo $this->input->post('data');
	}

	public function edit($id)
	{
		$data = $this->Credit->editCredit($id);
	    echo json_encode($data);
	}

	public function saveIssued()
	{
		$from = $this->input->post('min');
		$to = $this->input->post('max');
		$shop = $this->input->post('shop');
		$user = $this->input->post('user');
		$fb = '';
		for ($i=$from; $i <= $to; $i++) { 
			$db = $this->Credit->saveIssued($shop, $i, $user);
			$fb .= $db;
		}

		// foreach ($data as $key => $value) {
		// 	$this->Credit->saveIssued($shop, $value->id, $value->serial_number, $value->denomination);			
		// }
		echo json_encode($fb);
	}

	public function getCredit()
	{
		$credits = $this->Credit->getCredit('');
		$data = array();
	      foreach ($credits as $key => $v) {
	      	$qty = $this->Credit->getDenQty($v->id);
	         $data[] = array(
	            'id' => $v->id,
	            'denomination' => $v->denomination,
	            'qty' => $qty->total,
	            'wprice' => $v->wholesale_price,
	            'rprice' => $v->retail_price,
	            'bprice' => $v->buying_price
	         );
	      }

	      $output = [
	      	'data' => $data
	      ];
	    header('Content-Type: application/json');
	    echo json_encode( $output );
	}

	public function getaltransactions()
	{
		$from = $this->input->post('from');
        $to = $this->input->post('to');
        $person = $this->input->post('person');
        $ttype = $this->input->post('ttype');
        $shop = $this->input->post('shop');

        if($from!='' && $to!='')
        {
            $from = date('Y-m-d',strtotime($from));
            $to = date('Y-m-d',strtotime($to));
        }
		$credits['data'] = $this->Credit->getAlTransactions($from,$to, $person, $ttype, $shop);
		header('Content-Type: application/json');
	    echo json_encode( $credits );
	}

	public function getTransactionsSummary()
	{
		$from = $this->input->post('from');
        $to = $this->input->post('to');
        $person = $this->input->post('person');
        $ttype = $this->input->post('ttype');
        $shop = $this->input->post('shop');
		$denominations = $this->Credit->getDenominations();
		$data = [];
		foreach ($denominations as $key => $value) {
			$ttransactions = $this->Credit->getTTransactions($value->id, $from,$to, $person, $ttype, $shop);
			// $issued = $this->Credit->getIssued($value->id, $from,$to, $person, $ttype, $shop);
			$transactionR = $this->Credit->transactionTypesW($value->id, $from,$to, $person, 1, $shop);
			$transactionW = $this->Credit->transactionTypesW($value->id, $from,$to, $person, 2, $shop);
			if($ttransactions->quantity == null){
				$ttransactions->quantity = 0;
			}
			if($ttransactions->amount == null){
				$ttransactions->amount = 0;
			}
			if($transactionW->amount == null){
				$transactionW->amount = 0;
			}
			if($transactionW->quantity == null){
				$transactionW->quantity = 0;
			}
			if($transactionR->amount == null){
				$transactionR->amount = 0;
			}
			if($transactionR->quantity == null){
				$transactionR->quantity = 0;
			}

			$totalWBuyingPrice = $value->buying_price * $transactionW->quantity;
			$wTO = $transactionW->amount - $totalWBuyingPrice;
			$wTOPec = number_format((($wTO / $totalWBuyingPrice) * 100),1);
			$wTOPecVal = $wTO . ' ('.$wTOPec.'%)';

			$totalRBuyingPrice = $value->buying_price * $transactionR->quantity;
			$wTOR = $transactionR->amount - $totalRBuyingPrice;
			$wTORPec = number_format((($wTOR / $totalRBuyingPrice) * 100),1);
			$wTORPecVal = $wTOR . '('.$wTORPec.'%)';
			$data[] = [
				'denomination' => $value->denomination,
				'ttransactions' => $ttransactions->quantity,
				'wTrans'	=> $transactionW->quantity,
				'rTrans'	=> $transactionR->quantity,
				'wTO'	=> $wTOPecVal,
				'wTOR'	=> $wTORPecVal,
				'rTransA'	=> $transactionR->amount,
				'TotalWBPrice'	=> $totalWBuyingPrice,
				'TotalRBPrice'	=> $totalRBuyingPrice,
				'wTransA'	=> $transactionW->amount,
				'ttamount' => $ttransactions->amount,
				'issued' => $this->Credit->getIssued($value->id, $from,$to, $person, $ttype, $shop, 1),
				'Nissued' => $this->Credit->getIssued($value->id, $from,$to, $person, $ttype, $shop, 0)
			];
		}
		 $output = [
	      	'data' => $data
	      ];
		header('Content-Type: application/json');
	    echo json_encode( $output );
	}

	public function update()
	{
		$data = array(
	          'selling_price' => $this->input->post('selling_price'),
	          'buying_price' => $this->input->post('buying_price')
	      );
	    $update = $this->Credit->update($this->input->post('id'), $data);
	    echo json_encode("Updated Successfully!");
	}

}
?>
