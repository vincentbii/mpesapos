<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Secure_Controller.php");

/**
 * 
 */
class Mpesaadmin extends Secure_Controller
{
	public function __construct(){
		parent::__construct('mpesaadmin');
		$this->load->model('Mpesa_admin');
	}

	public function index($value='')
	{
		$output['persons'] = $this->Mpesa_admin->users();
		$this->load->view('mpesaadmin/manage', $output);
	}

	public function users($value='')
	{
		
		$output['persons'] = $this->Mpesa_admin->users();
		header('Content-Type: application/json');
		echo json_encode($output);
	}

	public function report($value='')
	{
		$to = $this->input->post('to');
		$from = $this->input->post('from');
		$person = $this->input->post('person');

		$report = $this->Mpesa_admin->get_last_ten_entries($person, $to, $from);
		$output = [
	      	'data' => $report
	      ];
		header('Content-Type: application/json');
		
		echo json_encode($output);
	}

	public function reports($value='')
	{
		$columns = array( 
                            0	=>'shop', 
                            1	=>'targets',
                            2	=> 'deposits',
                            3	=> 'withdrawals',
                            4	=> 'deficit',
                            5	=>	'opening_float',
                            6	=>	'available_cash',
                            7	=>	'report_date',
                            8	=>	'user'
                        );

		$limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir = $this->input->post('order')[0]['dir'];
  
        $totalData = $this->Mpesa_admin->alldata_count();
            
        $totalFiltered = $totalData; 
            
        if(empty($this->input->post('search')['value']))
        {            
            $posts = $this->Mpesa_admin->alldatas($limit,$start,$order,$dir);
        }
        else {
            $search = $this->input->post('search')['value']; 

            $posts =  $this->Mpesa_admin->data_search($limit,$start,$search,$order,$dir);

            $totalFiltered = $this->Mpesa_admin->datas_search_count($search);
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
            	$nestedData['user']	=	$post->user;
            	$nestedData['report_date']	=	$post->report_date;
                $nestedData['shop'] = $post->shop;
                $nestedData['targets'] = $post->targets;
                $nestedData['deposits'] = $post->deposits;
                $nestedData['withdrawals'] = $post->withdrawals;
                $nestedData['deficit'] = $post->deficit;
                $nestedData['available_cash'] = $post->available_cash;
                $nestedData['opening_float'] = $post->withdrawals;
                
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($this->input->post('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        echo json_encode($json_data); 
	}
}