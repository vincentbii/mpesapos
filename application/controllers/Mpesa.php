<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Secure_Controller.php");

class Mpesa extends Secure_Controller
{
	
	public function __construct()
	{
		parent::__construct('mpesa');
    	$this->load->model('MpesaReport');
    	$this->load->model('Stock_location');
	}

	public function index(){
    	$this->load->view('mpesa/manage');
	}

	public function edit($value='')
	{
		$data = [
			'target'	=>	$this->input->post('target'),
			'withdrawals'	=>	$this->input->post('withdrawals'),
			'deposits'	=>	$this->input->post('deposits'),
			'opening_float'	=>	$this->input->post('opening_float'),
			'available_cash'	=>	$this->input->post('available_cash'),
			'deficit'	=>	$this->input->post('deficit'),
			'user'		=> $this->session->userdata('person_id'),
			'report_date'	=>	date("Y-m-d")
		];
		$id = $this->input->post('location_id');
		$insert = $this->MpesaReport->edit($id, $data);
		echo json_encode($insert);
	}

	public function report($value='')
	{
		$Stock_location = $this->Stock_location->get_undeleted('sales');
		foreach ($Stock_location as $key => $value) {
			$this->MpesaReport->createEntries($value);
		}
		$disable = $this->MpesaReport->disable();
		$locations = $this->MpesaReport->getReport('sales');
		$output = [
	      	'data' => $locations
	      ];
		header('Content-Type: application/json');
		echo json_encode($output);
	}

}

?>