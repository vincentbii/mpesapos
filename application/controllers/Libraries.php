<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Secure_Controller.php");

/**
 * 
 */
class Libraries extends Secure_Controller
{
	
	function __construct()
	{
		parent::__construct('libraries');
		$this->load->model('Library2');
	}

	public function index($value='')
	{
		$this->load->view('library/manage');
	}

	public function download($value='')
	{
		$this->load->view('library/download_view');
	}

	public function files($value='')
	{
		$output = [
	      	'data' => $this->Library2->getFiles()
	      ];
		header('Content-Type: application/json');
		echo json_encode($output);
	}

	public function show($category='')
	{
		$data['files'] = $this->Library2->getFiles($category);

		$this->load->view('library/show', $data);
	}

	public function categories($value='')
	{
		$categories = $this->Library2->getCategories('');
		$data = array();
	      foreach ($categories as $key => $v) {
	         $data[] = array(
	            'id' => $v->id,
	            'name' => $v->name,
	            'description' => $v->description
	         );
	      }

	      $output = [
	      	'data' => $data
	      ];
	    header('Content-Type: application/json');
	    echo json_encode( $output );
	}

	public function downloadFile($value='')
	{
        $this->load->helper('download');
		$data = file_get_contents('./uploads/'.$value);
		force_download($value, $data);
	}
}