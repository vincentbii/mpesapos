<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once("Secure_Controller.php");

/**
 * 
 */
class Librarian extends Secure_Controller
{
	
	function __construct()
	{
		parent::__construct('librarian');
		$this->load->model('Library_admin');
		$this->load->helper(array('form', 'url'));
	}

	public function index($value='')
	{
		$this->load->view('library_admin/manage', array('error' => ' ' ));
	}

	public function categories($value='')
	{
		$categories = $this->Library_admin->getCategories($value);
		$output = [
	      	'data' => $categories
	      ];
		header('Content-Type: application/json');
		echo json_encode($output);
	}

	public function files($value='')
	{
		$output = [
	      	'data' => $this->Library_admin->getFiles()
	      ];
		header('Content-Type: application/json');
		echo json_encode($output);
	}

	public function edit($value='')
	{
		# code...
	}

	public function updateFile($value='')
	{
		$data = [
			'name' 		=> $this->input->post('name'),
			'category'	=> $this->input->post('category'),
			'description'	=>	$this->input->post('link_description')
		];
		// $category_name = $this->input->post('category_name');
		$id = $this->input->post('id');
		$update = $this->Library_admin->updateFile($id,$data);
		echo json_encode("File Updated Successfully!");
	}

	public function update($value='')
	{
		$data = [
			'name' => $this->input->post('name'),
			'description'	=>	$this->input->post('category_description')
		];
		// $category_name = $this->input->post('category_name');
		$id = $this->input->post('id');
		$update = $this->Library_admin->updateCategory($id,$data);
		echo json_encode("Category Updated Successfully!");
	}

	public function storecategory($value='')
	{
		$data = [
			'name' => $this->input->post('name'),
			'description'	=>	$this->input->post('category_description')
		];
		// $category_name = $this->input->post('category_name');
		$id = $this->input->post('id');
		$update = $this->Library_admin->storeCategory($id,$data);
		echo json_encode("Added Successfully!");
	}

	public function addLink($value='')
	{
		$store = [
        	'category'	=>	$this->input->post('category'),
        	'external_link'	=>	$this->input->post('external_link'),
        	'allow_links'	=>	$this->input->post('allow_links'),
        	'description'	=>	$this->input->post('link_description')
        ];
        $insert = $this->Library_admin->addLink($store);
        echo json_encode($store['allow_links']);
	}

	public function addFile($value='')
	{
		

		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'mp4|doc|docx|xlsx|pdf|png|jpg';
        $config['max_size']             = '0';
        $config['overwrite']			= FALSE;
		$config['remove_spaces']		=	TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ( ! $this->upload->do_upload('userfile'))
        {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('library_admin/manage', $error);
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());

                $store = [
                	'name'	=>	$data['upload_data']['file_name'],
                	'filepath'	=>	$config['upload_path'],
                	'category'	=>	$this->input->post('category'),
                	'external_link'	=>	$this->input->post('external_link'),
                	'allow_links'	=>	$this->input->post('allow_links'),
                	'description'	=>	$this->input->post('link_description')
                ];
                $insert = $this->Library_admin->addFile($this->input->post('id'), $store);
                $this->load->view('library_admin/upload_success', $data);
        }
	}

	public function destroy($value='')
	{
		$delete = $this->Library_admin->destroy($value);
		echo json_encode("Deleted Successfully!");
	}

	public function destroyFile($value='')
	{
		$delete = $this->Library_admin->destroyFile($value);
		echo json_encode($delete);
	}

	public function downloadFile($value='')
	{
        $this->load->helper('download');
		$data = file_get_contents('./uploads/'.$value);
		force_download($value, $data);
	}
}